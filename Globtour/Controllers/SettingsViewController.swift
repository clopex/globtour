//
//  SettingsViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/7/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SwiftyJSON

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var nameSurnameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var initialsLbl: UILabel!
    
    var isLogdIn = false
    var aplicationUser = AplicationUser()
    var currencyName = ""
    var langShort = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationImageLogo()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        checkForUserData()
    }
    
    func checkForUserData() {
        currencyName = UserDefaults.standard.getCurrencyName()
        langShort = UserDefaults.standard.getLanguageName()
        isLogdIn = UserDefaults.standard.isUserLogIn()
        if isLogdIn {
            circleView.isHidden = false
            let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
            aplicationUser = AplicationUser(json: userDataJSON)
            let name = aplicationUser.name ?? ""
            let surname = aplicationUser.surname ?? ""
            if !(name.isEmpty && surname.isEmpty) {
                let secondLetter = surname.first
                let firstLetter = name.first
                initialsLbl.text = "\(firstLetter!)\(secondLetter!)"
                initialsLbl.text = initialsLbl.text?.uppercased()
            } else {
                initialsLbl.text = ""
            }
            
            let email = aplicationUser.email ?? ""
            nameSurnameLbl.text = "\(name) \(surname)"
            emailLbl.text = email
            nameSurnameLbl.text = name + " " + surname
        } else {
            circleView.isHidden = true
            emailLbl.text = ""
            initialsLbl.text = ""
            nameSurnameLbl.text = ""
        }
        tableView.reloadData()
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    private func setupViews() {
        circleView.layer.cornerRadius = circleView.frame.size.width / 2
        circleView.clipsToBounds = true
        tableView.contentInset.bottom = 20
    }
    
    lazy var registrationCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "account_add")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "registration".localized()
        return cell
    }()
    
    lazy var loginCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "account")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "login".localized()
        return cell
    }()
    
    lazy var logoutCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "account_remove")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "logout".localized()
        return cell
    }()
    
    lazy var editCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "account_edit")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "edit".localized()
        return cell
    }()
    
    lazy var languageCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "language")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "lang".localized()
        return cell
    }()
    
    lazy var currencyCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "currency")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "currency".localized() //+ " " + currencyName
        return cell
    }()
    
    lazy var termsCell: SettingsTableViewCell = {
        let cell = SettingsTableViewCell.createNewInstance()
        cell.settingsImage.image = UIImage(named: "terms")
        cell.settingsImage.contentMode = .scaleAspectFit
        cell.settingsLabel.text = "terms".localized()
        return cell
    }()
    
    lazy var seperatorCell: SeperatorTableViewCell = {
        let cell = SeperatorTableViewCell.createNewInstance()
        return cell
    }()
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLogdIn {
            switch indexPath.row {
            case 0:
                return logoutCell
            case 1:
                return editCell
            case 2:
                return seperatorCell
            case 3:
                languageCell.settingsLabel.text = "lang".localized() + " " + langShort
                return languageCell
            case 4:
                currencyCell.settingsLabel.text = "currency".localized() + " " + currencyName
                return currencyCell
            case 5:
                return termsCell
            default:
                print("Error cell")
            }
            
            return UITableViewCell()
        } else {
            switch indexPath.row {
            case 0:
                return registrationCell
            case 1:
                return loginCell
            case 2:
                return seperatorCell
            case 3:
                return languageCell
            case 4:
                
                currencyCell.settingsLabel.text = "currency".localized() + " " + currencyName
                return currencyCell
            case 5:
                return termsCell
            default:
                print("Error cell")
            }
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isLogdIn {
            switch indexPath.row {
            case 0:
                isLogdIn = false
                UserDefaults.standard.setLogdIn(value: false)
                UserDefaults.standard.setUserData(value: "")
                circleView.isHidden = true
                nameSurnameLbl.text = ""
                emailLbl.text = ""
                initialsLbl.text = ""
                tableView.reloadData()
            case 1:
                let editViewController = EditProfileViewController()
                self.navigationController?.pushViewController(editViewController, animated: true)
            case 2:
                print("seperator")
            case 3:
                let translationsViewController = LanguageViewController()
                self.navigationController?.pushViewController(translationsViewController, animated: true)
            case 4:
                let currencyViewController = CurrenciesViewController()
                self.navigationController?.pushViewController(currencyViewController, animated: true)
            case 5:
                 print("Error cell")//return termsCell
            default:
                print("Error cell")
            }
        } else {
            switch indexPath.row {
            case 0:
                let registrationViewController = RegistrationViewController()
                self.navigationController?.pushViewController(registrationViewController, animated: true)
            case 1:
                let loginViewController = LoginViewController()
                self.navigationController?.pushViewController(loginViewController, animated: true)
            case 2:
                print("seperator")
            case 3:
                let translationsViewController = LanguageViewController()
                self.navigationController?.pushViewController(translationsViewController, animated: true)
            case 4:
                let currencyViewController = CurrenciesViewController()
                self.navigationController?.pushViewController(currencyViewController, animated: true)
            case 5:
             print("Error cell")//return termsCell
            default:
                print("Error cell")
            }
        }
    }
}






