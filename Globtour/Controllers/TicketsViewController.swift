//
//  TicketsViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/7/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toast_Swift
import STZPopupView

class TicketsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let cellId = "cellid"
    
    var aplicationUser = AplicationUser()
    let webAPIController = WebApiController()
    
    var tickets = [MyTicketsResponse]()
    var ticket = MyTicketsResponse()
    var isLogdIn = false
    var languageName = ""
    var isPicked = false
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationImageLogo()
        setupProgresView()
        setupNoDataView()
        webAPIController.delegate = self
        tableView.allowsSelection = false
        tableView.register(UINib(nibName: "TicketTableViewCell", bundle: nil),forCellReuseIdentifier: cellId)
        self.tableView.addSubview(self.refreshControl)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissReturnPicker))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        picker.removeFromSuperview()
    }
    
    @objc func dismissReturnPicker() {
        if isPicked {
            let langString = Locale.current.languageCode
            let currencyId = UserDefaults.standard.getCurrencyId()
            let stationFrom = ticket.stanSifp ?? 0
            let stationTo = ticket.stanSifd ?? 0
            let departureStation = ticket.stanNazp ?? ""
            let arrivalStation = ticket.stanNazd ?? ""
            let requestTrip = RequestModel(lang: langString!, stationFrom: stationFrom, stationTo: stationTo, dateTrip: dateStringFrom, dateReturn: dateStringFrom, isReturn: false, isReturnDateKnown: false, currency: currencyId, token: token)
            let departureViewController = DepartureViewController()
            departureViewController.requestTrip = requestTrip
            departureViewController.stationNameTo = departureStation
            departureViewController.stationNameFrom = arrivalStation
            departureViewController.ticket = ticket
            departureViewController.isFromTicket = true
            let backItem = UIBarButtonItem()
            backItem.title = "\(departureStation)-\(arrivalStation)"
            navigationItem.backBarButtonItem = backItem
            picker.removeFromSuperview()
            isPicked = false
            self.navigationController?.pushViewController(departureViewController, animated: true)
        }
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        activityIndicator.startAnimating()
    }
    
    var token: String {
        return Token.shared?.token ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getLogUser()
    }
    
    func getLogUser() {
        isLogdIn = UserDefaults.standard.isUserLogIn()
        if isLogdIn {
            noDataView.isHidden = true
            tableView.isHidden = false
            let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
            aplicationUser = AplicationUser(json: userDataJSON)
            languageName = UserDefaults.standard.getLanguageName()

            let userId = aplicationUser.id ?? 0
            let agencyId = aplicationUser.agencyId ?? 0
            let ticketRequest = MyTicketsRequest(userId: userId, agencyId: agencyId, lang: languageName, onlyNotReserved: false, token: token)
            self.webAPIController.getTickets(data: ticketRequest)
        } else {
            noDataView.isHidden = false
            tableView.isHidden = true
            activityIndicator.stopAnimating()
        }
    }
    
    func setupNoDataView() {
        self.view.addSubview(noDataView)
        _ = noDataView.anchor(self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 60)
        noDataView.addSubview(noDataImageView)
        noDataView.addSubview(noDataLabel)
        
        _ = noDataImageView.anchor(nil, left: noDataView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
        noDataImageView.centerYAnchor.constraint(equalTo: noDataView.centerYAnchor).isActive = true
        
        _ = noDataLabel.anchor(nil, left: noDataImageView.rightAnchor, bottom: nil, right: noDataView.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        noDataLabel.centerYAnchor.constraint(equalTo: noDataView.centerYAnchor).isActive = true
        noDataView.isHidden = true
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isLogdIn = UserDefaults.standard.isUserLogIn()
        if isLogdIn {
            getLogUser()
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(handleRefresh(_:)),for: UIControlEvents.valueChanged)
        refreshControl.tintColor = PRIMARY_COLOR
        return refreshControl
    }()
    
    let noDataView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1
        view.layer.borderColor = PRIMARY_COLOR.cgColor
        return view
    }()
    
    let noDataImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "warning")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let noDataLabel: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize14
        label.text = "not_signed_in_tickets_data".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    var dateStringFrom = ""
    let picker : UIDatePicker = UIDatePicker()

    @objc func dueDateChanged(sender:UIDatePicker){
        dateStringFrom = sender.date.toString(dateFormat: DATE_FORMAT)
        let testdate = sender.date
        let todayDate = Date()
        if testdate < todayDate {
            self.view.makeToast("date_erroor".localized(), duration: 0.8, position: .top)
        } else {
            isPicked = true
//            let langString = Locale.current.languageCode
//            let currencyId = UserDefaults.standard.getCurrencyId()
//            let stationFrom = ticket.stanSifp ?? 0
//            let stationTo = ticket.stanSifd ?? 0
//            let departureStation = ticket.stanNazp ?? ""
//            let arrivalStation = ticket.stanNazd ?? ""
//            let requestTrip = RequestModel(lang: langString!, stationFrom: stationFrom, stationTo: stationTo, dateTrip: dateStringFrom, dateReturn: dateStringFrom, isReturn: false, isReturnDateKnown: false, currency: currencyId, token: token)
//            let departureViewController = DepartureViewController()
//            departureViewController.requestTrip = requestTrip
//            departureViewController.stationNameTo = departureStation
//            departureViewController.stationNameFrom = arrivalStation
//            departureViewController.ticket = ticket
//            departureViewController.isFromTicket = true
//            let backItem = UIBarButtonItem()
//            backItem.title = "\(departureStation)-\(arrivalStation)"
//            navigationItem.backBarButtonItem = backItem
////            picker.removeFromSuperview()
//            self.navigationController?.pushViewController(departureViewController, animated: true)
        }
    }
    
    @objc func reserveTicket(sender: UIButton) {
        ticket = tickets[sender.tag]
        picker.datePickerMode = UIDatePickerMode.date
        let departureTime = ticket.polazDtm ?? ""
        let departureTimeMs = UIHelper.substring(string: departureTime, fromIndex: 6, toIndex: 19)
        let depratureDate = Date.init(timeIntervalSince1970: (departureTimeMs! / 1000.0))
        picker.minimumDate = depratureDate
        dateStringFrom = picker.date.toString(dateFormat: DATE_FORMAT)
        isPicked = true
        picker.addTarget(self, action: #selector(dueDateChanged), for: .valueChanged)
        picker.backgroundColor = UIColor.white
        self.view.addSubview(picker)
        _ = picker.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    @objc func showTicket() {
        print("show ticket")
    }
    
    @objc func sendEmail(recognizer: UITapGestureRecognizer) {
        if (recognizer.view as? UIImageView) != nil {
            ticket = tickets[(recognizer.view?.tag)!]
            let popupView = createPopupview()
            let popupConfig = STZPopupViewConfig()
            popupConfig.dismissTouchBackground = true
            popupConfig.dismissAnimation = .slideOutToBottom
            presentPopupView(popupView, config: popupConfig)
        }
    }
    
    lazy var buttonYes: UIButton = {
        let button = UIButton()
        button.setTitle("send_email".localized(), for: .normal)
        button.titleLabel?.font = UIHelper.defaultAppFont
        button.contentHorizontalAlignment = .left  //contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(confirmDialog), for: .touchUpInside)
        return button
    }()
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 64, height: 50))
        popupView.backgroundColor = UIColor.white
        
        popupView.addSubview(buttonYes)
        
        _ = buttonYes.anchor(nil, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        buttonYes.centerYAnchor.constraint(equalTo: popupView.centerYAnchor).isActive = true
        
        return popupView
    }
    
    @objc func confirmDialog() {
        let posSif = ticket.posSif ?? 0
        let faktBr = ticket.faktBrj ?? 0
        let year = ticket.faktGod ?? 0
        dismissPopupView()
        let inoviceEmail = PaymentCommitedInvoiceResponse(posSif: posSif, faktBroj: faktBr, year: year, softwareVersion: "iOS", userId: 0, token: token, regeneratePdf: true)
        activityIndicator.startAnimating()
        webAPIController.sendEmail(invoice: inoviceEmail)
    }
}

extension TicketsViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let ticketResponse = result as? [MyTicketsResponse] {
            self.tickets = ticketResponse
            self.tableView.reloadData()
            refreshControl.endRefreshing()
            activityIndicator.stopAnimating()
        }
        
        if let responseCode = result as? HTTPURLResponse {
            if responseCode.statusCode == 200 {
                DispatchQueue.main.async {
                    self.view.makeToast("send_email_msg".localized(), duration: 2.0, position: .bottom)
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        print(error?.localizedDescription ?? "")
        activityIndicator.stopAnimating()
    }
}

extension TicketsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? TicketTableViewCell {
            let index = tickets[indexPath.row]
            
            if index.rezerviran! {
                cell.showBtn.tag = indexPath.row
                cell.showBtn.addTarget(self, action: #selector(showTicket), for: .touchUpInside)
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.sendEmail(recognizer:)))
                cell.dotsImage.isUserInteractionEnabled = true
                cell.dotsImage.tag = indexPath.row
                cell.dotsImage.addGestureRecognizer(tap)
            } else {
                cell.showBtn.tag = indexPath.row
//                self.ticket = index
                cell.showBtn.addTarget(self, action: #selector(reserveTicket), for: .touchUpInside)
            }
            cell.updateUI(item: index)
            
            return cell
        }
        
        return UITableViewCell()
    }
}



















