//
//  LanguageViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/17/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import Localize_Swift

class LanguageViewController: UITableViewController {
    
    let cellId = "cellId"
    
    var translations = [Language]()
    
    let webAPIController = WebApiController()
    let languages = ["hr".localized(), "sr".localized(), "en".localized(), "it".localized(), "de".localized(), "pl".localized()]
    let langShort = ["hr", "sr", "en", "it", "de", "pl"]
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webAPIController.delegate = self
        tableView.register(UINib(nibName: "CurrencyTableViewCell", bundle: nil),forCellReuseIdentifier: cellId)
        tableView.separatorInset = UIEdgeInsets.zero
//        setupProgresView()
        activityIndicator.startAnimating()
        webAPIController.getAllTranslations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CurrencyTableViewCell
//        let index = self.languages[indexPath.row]
        cell?.nameLabel.text = languages[indexPath.row]
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let langShort = self.langShort[indexPath.row]
        UserDefaults.standard.setLangaugeName(value: langShort)
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension LanguageViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let data = result as? [Language] {
            
//            self.translations = data
            self.translations = Array(Set(data))
            tableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        let title = "Error"
        let msg = "Greska prilikom dohvacanja jezika!"
        activityIndicator.stopAnimating()
        showAlert(title: title, message: msg)
    }
}

