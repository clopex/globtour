//
//  StationsViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/7/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class StationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var searchController = UISearchController(searchResultsController: nil)
    let webAPIController = WebApiController()
    var stations = [Station]()
    var filterdStations = [Station]()
    weak var delegate: StationDelegate?
    var isFromSearch = false
    var stationId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        setupNavigationImageLogo()
        webAPIController.delegate = self
        
//        if allStations.count > 0 {
//            filterdStations = allStations
//        } else {
        
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        filterdStations.removeAll()
        tableView.reloadData()
        activityIndicator.startAnimating()
        webAPIController.getAllStations(id: stationId)
    }

    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }

}

extension StationsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterdStations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as? StationsTableViewCell {
            let index = self.filterdStations[indexPath.row]
            cell.updateName(item: index)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isFromSearch {
            delegate?.pickerDidFinishWithResult(sender: self, pickerItems: filterdStations[indexPath.row])
            isFromSearch = false
            navigationController?.popToRootViewController(animated: true)
        } else {
//            print(stations[indexPath.row].id)
            let stationMapVC = StationMapViewController()
            stationMapVC.stationForMark = filterdStations[indexPath.row]
            self.navigationController?.pushViewController(stationMapVC, animated: true)
        }
    }
}

extension StationsViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        self.stations = result as! [Station]
        filterdStations = stations
        allStations.removeAll()
        allStations = stations
        tableView.reloadData()
        activityIndicator.stopAnimating()
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        print("Fetch Stations Error: \(String(describing: error))")
        activityIndicator.stopAnimating()
    }
}

extension StationsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text! == "" {
            filterdStations = stations
            //allStations.removeAll()
//            allStations = stations
        } else {
            filterdStations = stations.filter{ ($0.name?.lowercased().contains(self.searchController.searchBar.text!.lowercased()))!}
        }
        self.tableView.reloadData()
    }

}

