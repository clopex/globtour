//
//  EditProfileViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/28/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SnapKit
import CountryPicker
import SwiftyJSON
import Localize_Swift
import Toast_Swift

class EditProfileViewController: UITableViewController {
    
    var btnSave: UIButton!
    let webAPIController = WebApiController()
    var aplicationUser = AplicationUser()
    var passwordData = Password()
    var newUserData: AplicationUser!
    var alertVC = UIAlertController()
    
    var email = ""
    var password = ""
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    lazy var countryPickerView: CountryPicker = {
        let picker = CountryPicker()
        picker.showPhoneNumbers = true
        picker.backgroundColor = UIColor.white
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        picker.setCountry(code!)
        picker.isHidden = true
        return picker
    }()
    
    var token: String {
        return Token.shared?.token ?? ""
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        getUserData()
        setupNavigationImageLogo()
        webAPIController.delegate = self
        countryPickerView.countryPickerDelegate = self
        setupProgresView()
        setupViews()
    }
    
    func getUserData() {
        let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
        aplicationUser = AplicationUser(json: userDataJSON)
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
    
    func setupViews() {
        // contryPicker
        self.navigationController?.view.addSubview(countryPickerView)
        //        self.view.addSubview(countryPickerView)
        _ = countryPickerView.anchor(nil, left: self.navigationController?.view.leftAnchor, bottom: self.navigationController?.view.bottomAnchor, right: self.navigationController?.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        // tableview setup
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.contentInset.top = 50
        tableView.contentInset.bottom = 20
        
        // table footer view setup
        let tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        btnSave = UIButton(type: .custom)
        btnSave.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
        btnSave.backgroundColor = PRIMARY_COLOR
        btnSave.setTitleColor(.white, for: .normal)
        btnSave.setTitle("save_changes".localized(), for: .normal)
        btnSave.clipsToBounds = true
        btnSave.titleLabel?.font = UIHelper.defaultAppFont
        btnSave.layer.cornerRadius = 2
        tableFooterView.addSubview(btnSave)
        
        btnSave.snp.makeConstraints { (make) in
            make.leading.equalTo(tableFooterView).offset(16)
            make.trailing.equalTo(tableFooterView).offset(-16)
            make.top.equalTo(tableFooterView).offset(32)
            make.bottom.equalTo(tableFooterView).offset(0)
        }
        tableView.tableFooterView = tableFooterView
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func dismissKeyboard(_ sender: Any) {
        countryPickerView.isHidden = true
        view.endEditing(true)
    }
    
    func validate() -> Bool {
        var valid = true
        
        let name = nameCell.inputTextField.text!
        let surname = surnameCell.inputTextField.text!
        let email = emailCell.inputTextField.text!
        let password = passwordCell.cellTextField.text!
        let phone = phoneCell.inputTextField.text!
        
        if (name.isEmpty) {
            nameCell.showWrong()
            valid = false
        } else {
            nameCell.hideWrong()
        }
        
        if surname.isEmpty {
            surnameCell.showWrong()
            valid = false
        } else {
            surnameCell.hideWrong()
        }
        
        if email.isEmpty {
            emailCell.showWrong()
            valid = false
        } else {
            if !emailCell.isEmailValid() {
                valid = false
            }
        }
        
        if phone.isEmpty {
            phoneCell.showWrong()
            valid = false
        } else if phone.count < 8 {
            phoneCell.showWrong()
            let title = "Error"
            let msg = "phone_error".localized()
            showAlert(title: title, message: msg)
        } else {
            if !phoneCell.validatePhone(value: phone) {
                valid = false
            }
        }
        
        if password.isEmpty {
            passwordCell.showWrong()
            valid = false
        } else if password.count < 8 {
            let title = "Error"
            let msg = "password_lenght_err".localized()
            showAlert(title: title, message: msg)
            passwordCell.showWrong()
            valid = false
        }
        
        return valid
    }
    
    @objc func saveAction(_ sender: Any) {
        if validate() {
            activityIndicator.startAnimating()
            let email = emailCell.inputTextField.text!
            var phoneNum = phoneCell.inputTextField.text!
            phoneNum.remove(at: phoneNum.startIndex)
            let name = nameCell.inputTextField.text!
            let surname = surnameCell.inputTextField.text!

            guard let userID = self.aplicationUser.id else {return}
            guard let agenAdr = self.aplicationUser.aGEN_ADR else {return}
            guard let agenNaz = self.aplicationUser.aGEN_NAZ else {return}
            guard let agenOib = self.aplicationUser.aGEN_OIB else {return}
            guard let emailConf = self.aplicationUser.emailConfirmed else {return}
            guard let accessFaild = self.aplicationUser.accessFailedCount else {return}
            guard let secureityStamp = self.aplicationUser.securityStamp else {return}
            guard let lockOutEnd = self.aplicationUser.lockoutEndDateUtc else {return}
            guard let lockoutEnabled = self.aplicationUser.lockoutEnabled else {return}
            guard let phoneNumConf = self.aplicationUser.phoneNumberConfirmed else {return}
            guard let marketing = self.aplicationUser.marketing else {return}
            guard let agencyId = self.aplicationUser.agencyId else {return}
            guard let posId = self.aplicationUser.posId else {return}

            self.email = email
            self.password = passwordCell.cellTextField.text!

            newUserData = AplicationUser(_agenAdr: agenAdr, _agenNaz: agenNaz, _agenOib: agenOib, _tableName: "APPLICATION_USERS", _securityStamp: secureityStamp, _lockoutEndDateUtc: lockOutEnd, _acceptTerms: true, _email: email, _emailConfirmed: emailConf, _accessFailedCount: accessFaild, _id: userID, _lockoutEnabled: lockoutEnabled, _passwordHash: "", _phoneNumber: phoneNum, _phoneNumberConfirmed: phoneNumConf, _username: email, _name: name, _surname: surname, _marketing: marketing, _token: token, _agencyId: agencyId, _posId: posId)

            webAPIController.changeProfileInformation(userData: newUserData)
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            if let name = aplicationUser.name {
                nameCell.inputTextField.text = name
            }
            return nameCell
        case 1:
            if let surname = aplicationUser.surname {
                surnameCell.inputTextField.text = surname
            }
            return surnameCell
        case 2:
            if let email = aplicationUser.email {
                emailCell.inputTextField.text = email
            }
            emailCell.inputTextField.isEnabled = false
            return emailCell
        case 3:
            if let phoneNum = aplicationUser.phoneNumber {
                phoneCell.inputTextField.text = "+\(phoneNum)"
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(openCountryPickerDialog))
            phoneCell.flagImage.addGestureRecognizer(tap)
            return phoneCell
        case 4:
            passwordCell.cellButton.addTarget(self, action: #selector(changePasswordDialog(_:)), for: .touchUpInside)
            return passwordCell
        default:
            print("Error in Registratin TableView")
        }
        return UITableViewCell()
    }
    
    @objc func openCountryPickerDialog() {
        countryPickerView.isHidden = false
    }
    
    @objc func changePasswordDialog(_ sender: Any) {
        alertVC = UIAlertController(title: "password_change".localized(), message: nil, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "close".localized(), style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: "change".localized(), style: .default, handler: { alert -> Void in
            let passwordField = self.alertVC.textFields![0] as UITextField
            let repeatPasswordField = self.alertVC.textFields![1] as UITextField
            let newPasswordField = self.alertVC.textFields![2] as UITextField
            
            if passwordField.text == "" || repeatPasswordField.text == "" || newPasswordField.text == "" {
                let errorAlert = UIAlertController(title: "Error", message: "Please input all fields", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                    alert -> Void in
                    self.present(self.alertVC, animated: true, completion: nil)
                }))
                self.present(errorAlert, animated: true, completion: nil)
            } else if !(repeatPasswordField.text == newPasswordField.text) {
                let errorAlert = UIAlertController(title: "Error", message: "not_same_passwords".localized(), preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                    alert -> Void in
                    self.present(self.alertVC, animated: true, completion: nil)
                }))
                self.present(errorAlert, animated: true, completion: nil)
            } else {
                guard let userID = self.aplicationUser.id else {return}
                guard let oldPassword = passwordField.text else {return}
                guard let newPassword = newPasswordField.text else {return}
                
                self.activityIndicator.startAnimating()
                let passwordData = Password(_id: userID, _oldPassword: oldPassword, _newPassword: newPassword, _token: self.token)
                self.webAPIController.changePassword(passData: passwordData)
            }
            
        }))
        
        alertVC.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "old_pass".localized()
            textField.isSecureTextEntry = true
        })
        
        alertVC.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "new_pass".localized()
            textField.isSecureTextEntry = true
        })
        
        alertVC.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "new_pass_repeat".localized()
            textField.isSecureTextEntry = true
        })
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    lazy var nameCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "name".localized()
        cell.accountImage.image = UIImage(named: "account_orange")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var surnameCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "surname".localized()
        cell.accountImage.image = UIImage(named: "account_orange")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var emailCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "email".localized()
        cell.accountImage.isUserInteractionEnabled = true
        cell.accountImage.image = UIImage(named: "email")
        cell.inputTextField.keyboardType = .emailAddress
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var phoneCell: PhoneTableViewCell = {
        let cell = PhoneTableViewCell.createNewInstance()
        cell.accountLbl.text = "phone".localized()
        let imageData = UserDefaults.standard.getCountryFlag()
        let dataDecode:NSData = NSData(base64Encoded: imageData, options:.ignoreUnknownCharacters)!
        let avatarImage:UIImage = UIImage(data: dataDecode as Data)!
        cell.flagImage.image = avatarImage
        cell.flagImage.isUserInteractionEnabled = true
        cell.inputTextField.keyboardType = .phonePad
        cell.accountImage.image = UIImage(named: "phone")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var passwordCell: PasswordTableViewCell = {
        let cell = PasswordTableViewCell.createNewInstance()
        cell.cellLbl.text = "password".localized()
        cell.cellButton.setTitle("change_password".localized(), for: .normal)
        cell.cellTextField.isSecureTextEntry = true
        cell.cellImage.image = UIImage(named: "lock")
        cell.cellImage.contentMode = .scaleAspectFit
        return cell
    }()
}

extension EditProfileViewController: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        print(flag)
        self.countryPickerView.isHidden = true
        self.phoneCell.flagImage.image = flag
        self.phoneCell.inputTextField.text = phoneCode
    }
}

extension EditProfileViewController: WebApiControllerDelegate {
    
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        
        if let editProfileSuccess = result as? String {
            if editProfileSuccess == "Changed" {
                // toast with a specific duration and position
                self.navigationController?.view.makeToast("change_profile_success".localized(), duration: 1.5, position: .bottom)
                self.webAPIController.checkUserLogin(email: email, password: password)
            } else {
                if let userData = result as? String {
                    UserDefaults.standard.setLogdIn(value: true)
                    UserDefaults.standard.setUserData(value: userData)
                    self.activityIndicator.stopAnimating()
                    navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        if let changePassword = result as? Bool {
            if changePassword {
                self.navigationController?.view.makeToast("change_password_success".localized(), duration: 1.5, position: .bottom)
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        if error?.localizedDescription == ERROR_MSG {
            self.navigationController?.view.makeToast("Error", duration: 1.5, position: .top)
        }
        self.activityIndicator.stopAnimating()
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var contentOffset:CGPoint = self.tableView.contentOffset
        contentOffset.y  = 80
        UIView.animate(withDuration: 0.1) {
            self.tableView.contentOffset = contentOffset
        }
        
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var contentOffset:CGPoint = self.tableView.contentOffset
        contentOffset.y  = 0
        UIView.animate(withDuration: 0.1) {
            self.tableView.contentOffset = contentOffset
        }
    }
}





