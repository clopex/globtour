//
//  RegistrationViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SnapKit
import CountryPicker

class RegistrationViewController: UITableViewController {
    
    var btnSave: UIButton!
    let webAPIController = WebApiController()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    lazy var countryPickerView: CountryPicker = {
        let picker = CountryPicker()
        picker.showPhoneNumbers = true
        picker.backgroundColor = UIColor.white
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        picker.setCountry(code!)
        picker.isHidden = true
        return picker
    }()
    
    var token: String {
        return Token.shared?.token ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationImageLogo()
        webAPIController.delegate = self
        countryPickerView.countryPickerDelegate = self
        setupProgresView()
        setupViews()
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupViews() {
        // contryPicker
        self.navigationController?.view.addSubview(countryPickerView)
        //        self.view.addSubview(countryPickerView)
        _ = countryPickerView.anchor(nil, left: self.navigationController?.view.leftAnchor, bottom: self.navigationController?.view.bottomAnchor, right: self.navigationController?.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        // tableview setup
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.contentInset.top = 50
        tableView.contentInset.bottom = 20
        
        // table footer view setup
        let tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        btnSave = UIButton(type: .custom)
        btnSave.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
        btnSave.backgroundColor = PRIMARY_COLOR
        btnSave.setTitleColor(.white, for: .normal)
        btnSave.setTitle("sign_up".localized(), for: .normal)
        btnSave.clipsToBounds = true
        btnSave.titleLabel?.font = UIHelper.defaultAppFont
        btnSave.layer.cornerRadius = 2
        tableFooterView.addSubview(btnSave)
        
        btnSave.snp.makeConstraints { (make) in
            make.leading.equalTo(tableFooterView).offset(16)
            make.trailing.equalTo(tableFooterView).offset(-16)
            make.top.equalTo(tableFooterView).offset(32)
            make.bottom.equalTo(tableFooterView).offset(0)
        }
        tableView.tableFooterView = tableFooterView
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = false
        self.navigationController?.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func dismissKeyboard(_ sender: Any) {
        countryPickerView.isHidden = true
        view.endEditing(true)
    }
    
    @objc func saveAction(_ sender: Any) {
        if termsCell.isTermsAccepted() {
            if validate() {
                activityIndicator.startAnimating()
                let email = emailCell.inputTextField.text!
                let password = passwordCell.inputTextField.text!
                let phoneNum = phoneCell.inputTextField.text!
                let name = nameCell.inputTextField.text!
                let surname = surnameCell.inputTextField.text!
                
                let userData = AplicationUser(_acceptTerms: true, _email: email, _emailConfirmed: false, _accessFailedCount: 0, _id: 0, _lockoutEnabled: false, _passwordHash: password, _phoneNumber: phoneNum, _phoneNumberConfirmed: false, _username: email, _name: name, _surname: surname, _marketing: false, _token: token, _agencyId: 0, _posId: 0)
                
                webAPIController.registerUser(userData: userData)
            }
        } else {
            let title = "Error"
            let msg = "terms".localized()
            showAlert(title: title, message: msg)
        }
    }
    
    func validate() -> Bool {
        var valid = true
        
        let name = nameCell.inputTextField.text!
        let surname = surnameCell.inputTextField.text!
        let email = emailCell.inputTextField.text!
        let password = passwordCell.inputTextField.text!
        let repeatPassword = passwordRepeatCell.inputTextField.text!
        let phone = phoneCell.inputTextField.text!
        
        if (name.isEmpty) {
            nameCell.showWrong()
            valid = false
        } else {
            nameCell.hideWrong()
        }
        
        if surname.isEmpty {
            surnameCell.showWrong()
            valid = false
        } else {
            surnameCell.hideWrong()
        }
        
        if email.isEmpty {
            emailCell.showWrong()
            valid = false
        } else {
            if !emailCell.isEmailValid() {
                valid = false
            }
        }
        
        if phone.isEmpty {
            phoneCell.showWrong()
            valid = false
        } else if phone.count < 12 {
            phoneCell.showWrong()
            let title = "Error"
            let msg = "password_lenght_err".localized()
            showAlert(title: title, message: msg)
        } else {
            if !phoneCell.validatePhone(value: phone) {
                valid = false
            }
        }
        
        if password.isEmpty {
            passwordCell.showWrong()
            valid = false
        } else {
            passwordCell.hideWrong()
        }
        
        if repeatPassword.isEmpty {
            passwordRepeatCell.showWrong()
            valid = false
        } else {
            passwordRepeatCell.hideWrong()
        }
        
        if (password != repeatPassword) {
            valid = false
            passwordCell.showWrong()
            passwordRepeatCell.showWrong()
        } else {
            passwordCell.hideWrong()
            passwordRepeatCell.hideWrong()
        }
        
        return valid
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return nameCell
        case 1:
            return surnameCell
        case 2:
            return emailCell
        case 3:
            let tap = UITapGestureRecognizer(target: self, action: #selector(openCountryPickerDialog))
            phoneCell.flagImage.addGestureRecognizer(tap)
            return phoneCell
        case 4:
            passwordCell.inputTextField.delegate = self
            return passwordCell
        case 5:
            passwordRepeatCell.inputTextField.delegate = self
            return passwordRepeatCell
        case 6:
            return termsCell
        default:
            print("Error in Registratin TableView")
        }
        return UITableViewCell()
    }
    
    @objc func openCountryPickerDialog() {
        countryPickerView.isHidden = false
    }
    
    lazy var nameCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "name".localized()
        cell.accountImage.image = UIImage(named: "account_orange")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var surnameCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "surname".localized()
        cell.accountImage.image = UIImage(named: "account_orange")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var emailCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "email".localized()
        cell.accountImage.isUserInteractionEnabled = true
        cell.accountImage.image = UIImage(named: "email")
        cell.inputTextField.keyboardType = .emailAddress
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var phoneCell: PhoneTableViewCell = {
        let cell = PhoneTableViewCell.createNewInstance()
        cell.accountLbl.text = "phone".localized()
        cell.flagImage.image = UIImage(named: "language")
        cell.flagImage.isUserInteractionEnabled = true
        cell.inputTextField.keyboardType = .phonePad
        cell.accountImage.image = UIImage(named: "phone")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var passwordCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "password".localized()
        cell.accountImage.image = UIImage(named: "lock")
        cell.inputTextField.isSecureTextEntry = true
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var passwordRepeatCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "new_pass_repeat".localized()
        cell.inputTextField.isSecureTextEntry = true
        cell.accountImage.image = UIImage(named: "lock")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var termsCell: SwitchTableViewCell = {
        let cell = SwitchTableViewCell.createNewInstance()
        cell.termsLbl.text = "accepting_terms".localized()
        return cell
    }()
    
    var isRegister = true
    var dataImage = ""
}

extension RegistrationViewController: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        let imageData:NSData = UIImagePNGRepresentation(flag)! as NSData
        dataImage = imageData.base64EncodedString(options: .lineLength64Characters)
        self.countryPickerView.isHidden = true
        self.phoneCell.flagImage.image = flag
        self.phoneCell.inputTextField.text = phoneCode
    }
}

extension RegistrationViewController: WebApiControllerDelegate {
    
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        
        if self.isRegister {
            if let email = self.emailCell.inputTextField.text {
                if let password = self.passwordCell.inputTextField.text {
                    self.isRegister = false
                    self.webAPIController.checkUserLogin(email: email, password: password)
                }
            }
        } else {
            if let userData = result as? String {
                print(userData)
                UserDefaults.standard.setLogdIn(value: true)
                UserDefaults.standard.setUserData(value: userData)
                UserDefaults.standard.setCountryFlag(value: dataImage)
                self.activityIndicator.stopAnimating()
                navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        if error?.localizedDescription == ERROR_MSG {
            let title = "Error"
            let msg = "user_exists".localized()
            showAlert(title: title, message: msg)
        }
        self.activityIndicator.stopAnimating()
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var contentOffset:CGPoint = self.tableView.contentOffset
        contentOffset.y  = 80
        UIView.animate(withDuration: 0.1) {
            self.tableView.contentOffset = contentOffset
        }
        
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var contentOffset:CGPoint = self.tableView.contentOffset
        contentOffset.y  = 0
        UIView.animate(withDuration: 0.1) {
            self.tableView.contentOffset = contentOffset
        }
    }
}

