////
////  PickerViewController.swift
////  Globtour
////
////  Created by Adis Mulabdic on 12/9/17.
////  Copyright © 2017 Adis Mulabdic. All rights reserved.
////
//
//import UIKit
//
//class PickerViewController: UIViewController {
//    
//    var pickerItems: [Station] = []
//    var filteredPickerItems: [Station]!
//    var isSearchPerformed = false
//    var didSetupConstraints = false
//    var viewDidLayoutSubviewsOnceFlag = true
//    let cellReuseID = "cell"
//    var webAPIController: WebApiController?
//    var shouldFetchFormWebAPI = false
//    var selectedPickerItems: [Station] = []
//    weak var delegate: StationDelegate?
//    
//    lazy var tableView: UITableView = {
//        let tableView = UITableView()
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.backgroundColor = .white
//        tableView.backgroundView = UIView()
//        tableView.register(StationsTableViewCell.self, forCellReuseIdentifier: self.cellReuseID)
//        tableView.tableFooterView = UIView()
////        tableView.tintColor = UIHelper.navigationBarBackgroundColor
////        tableView.separatorColor = UIColor("#afafaf")!
//        return tableView
//    }()
//    
//    lazy var searchBar: UISearchBar = {
//        let searchBar = UISearchBar()
//        searchBar.delegate = self
//        searchBar.sizeToFit()
//        searchBar.searchBarStyle = .prominent
//        searchBar.isTranslucent = false
////        searchBar.barTintColor = UIHelper.navigationBarBackgroundColor
//        searchBar.tintColor = UIColor.white
//        searchBar.backgroundImage = UIImage()
//        searchBar.placeholder = "Search"
//        searchBar.returnKeyType = .done
//        return searchBar
//    }()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        var barButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissCancel))
//        navigationItem.leftBarButtonItem = barButton
//        barButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissSelected))
//        barButton.isEnabled = false
//        navigationItem.rightBarButtonItem = barButton
//        
//        tableView.backgroundColor = .white
//        tableView.register(StationsTableViewCell.self, forCellReuseIdentifier: cellReuseID)
//        tableView.tableFooterView = UIView()
////        tableView.tintColor = UIHelper.navigationBarBackgroundColor
////        tableView.separatorColor = UIColor("#afafaf")!
//
//        if allStations.count == 0 {
//            webAPIController?.getAllStations()
//        } else {
//            pickerItems = allStations
//        }
//    }
//    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        if viewDidLayoutSubviewsOnceFlag {
//            viewDidLayoutSubviewsOnceFlag = false
//        }
//    }
//
//
//}
//
//extension PickerViewController: UITableViewDataSource, UITableViewDelegate {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickerItems.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        return UITableViewCell()
//    }
//}
//
//extension PickerViewController: UISearchBarDelegate {
//    
//}

