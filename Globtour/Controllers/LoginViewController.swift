//
//  LoginViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/10/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UITableViewController {
    
    var btnSave: UIButton!
    let webAPIController = WebApiController()
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationImageLogo()
        webAPIController.delegate = self
        setupViews()
        setupProgresView()
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupViews() {
        // tableview setup
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.contentInset.top = 150
        tableView.contentInset.bottom = 20
        
        // table footer view setup
        let tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        btnSave = UIButton(type: .custom)
        btnSave.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
        btnSave.backgroundColor = PRIMARY_COLOR
        btnSave.setTitleColor(.white, for: .normal)
        btnSave.setTitle("sign_in_btn".localized(), for: .normal)
        btnSave.clipsToBounds = true
        btnSave.titleLabel?.font = UIHelper.defaultAppFont
        btnSave.layer.cornerRadius = 2
        tableFooterView.addSubview(btnSave)
        btnSave.snp.makeConstraints { (make) in
            make.leading.equalTo(tableFooterView).offset(16)
            make.trailing.equalTo(tableFooterView).offset(-16)
            make.top.equalTo(tableFooterView).offset(32)
            make.bottom.equalTo(tableFooterView).offset(0)
        }
        tableView.tableFooterView = tableFooterView
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func dismissKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
    @objc func saveAction(_ sender: Any) {
        if validate() {
            activityIndicator.startAnimating()
            let email = emailCell.inputTextField.text
            let password = passwordCell.inputTextField.text
            webAPIController.checkUserLogin(email: email!, password: password!)
        }
    }
    
    func validate() -> Bool {
        var valid = true
 
        let email = emailCell.inputTextField.text!
        let password = passwordCell.inputTextField.text!
        
        if email.isEmpty {
            emailCell.showWrong()
            valid = false
        } else {
            if !emailCell.isEmailValid() {
                valid = false
            }
        }
        
        if password.isEmpty {
            passwordCell.showWrong()
            valid = false
        } else {
            passwordCell.hideWrong()
        }
        
        return valid
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return emailCell
        case 1:
            return passwordCell
        default:
            print("Error in Registratin TableView")
        }
        return UITableViewCell()
    }
    
    lazy var emailCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "email".localized()
        cell.accountImage.image = UIImage(named: "email")
        cell.inputTextField.keyboardType = .emailAddress
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
    
    lazy var passwordCell: RegistrationTableViewCell = {
        let cell = RegistrationTableViewCell.createNewInstance()
        cell.accountLbl.text = "password".localized()
        cell.inputTextField.isSecureTextEntry = true
        cell.accountImage.image = UIImage(named: "lock")
        cell.accountImage.contentMode = .scaleAspectFit
        return cell
    }()
}

extension LoginViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        
        if let userData = result as? String {
            UserDefaults.standard.setLogdIn(value: true)
            UserDefaults.standard.setUserData(value: userData)
        }
        activityIndicator.stopAnimating()
        navigationController?.popToRootViewController(animated: true)
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        let title = "Error"
        let msg = "failed_login".localized()
        showAlert(title: title, message: msg)
        activityIndicator.stopAnimating()
    }

}
