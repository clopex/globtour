//
//  DepartureViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/11/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import STZPopupView
import SwiftyJSON
import Toast_Swift

class DepartureViewController: UIViewController {
    
    let cellId = "cell"
    let cellCollectionId = "cellId"
    let topCollectionId = "cellIdTop"
    let headerId = "headerId"
    let headerEmptyId = "headerEmptyId"
    
    var requestTrip: RequestModel!
    var ticket: MyTicketsResponse!
    let webAPIController = WebApiController()
    var departures: BaseDeparture!
    var departureFrom: [Departure] = []
    var departureTo: [Departure] = []
    var topDays: [DayList] = []
    
    var stationNameFrom = ""
    var stationNameTo = ""
    var currentDateString = ""
    var isFromTicket = false
    var isDateLarger = false
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        currentDateString = requestTrip._dateTrip
        setupProgresView()
        setupNoDataView()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: "DepartureTableViewCell", bundle: nil),forCellReuseIdentifier: cellId)
        setupCollectionViews()
        getNextSevenDays()
        webAPIController.delegate = self
        webAPIController.getDepartures(stationFrom: requestTrip._stationFrom, stationTo: requestTrip._stationTo, dateTrip: requestTrip._dateTrip, dateReturn: requestTrip._dateReturn, isReturn: requestTrip._isReturn, isReturnDateKnown: requestTrip._isReturnDateKnown)
    }
    
    var token: String {
        return Token.shared?.token ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "arrival_one".localized()
    }
    
    func getNextSevenDays() {
        let dateToCompare = currentDateString.toDate(dateFormat: DATE_FORMAT)
        let minDate = Calendar.current.date(byAdding: .day, value: -8, to: dateToCompare)
        let todayDate = Date()
        
        if todayDate < dateToCompare {
            for i in 0...7 {
                let tempDate = Calendar.current.date(byAdding: .day, value: i, to: minDate!)
                let dateToCompareString = tempDate?.toString(dateFormat: DATE_FORMAT)
                if dateToCompareString == currentDateString {
                    self.topDays.append(DayList(date: tempDate!, isSelected: true))
                    indexSelected = i
                } else {
                    self.topDays.append(DayList(date: tempDate!, isSelected: false))
                }
            }
        }
        
        if todayDate < dateToCompare {
            for item in topDays {
                let order = Calendar.current.compare(item.date, to: todayDate, toGranularity: .day)
                switch order {
                case .orderedAscending:
                    topDays = topDays.filter { $0 != item }
                case .orderedDescending:
                    print("drugo ")
                case .orderedSame:
                    print("samoe")
                }
            }
        }
        for i in 0...8 {
            let tempDate = Calendar.current.date(byAdding: .day, value: i, to: dateToCompare)
            let dateToCompareString = tempDate?.toString(dateFormat: DATE_FORMAT)
            if dateToCompareString == currentDateString {
                self.topDays.append(DayList(date: tempDate!, isSelected: true))
                indexSelected = i
            } else {
                self.topDays.append(DayList(date: tempDate!, isSelected: false))
            }
        }
        var index = 0
        for i in topDays {
            if i.isSelected {
                indexSelected = index
            }
            index = index + 1
        }
    }
    
    func setupCollectionViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsSelection = true
        collectionView.register(UINib(nibName: "SettingsLauncherCell", bundle: nil),forCellWithReuseIdentifier: cellCollectionId)
        collectionView.register(HeaderCollectionView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        
        topCollectionView.dataSource = self
        topCollectionView.delegate = self
        topCollectionView.allowsSelection = true
        topCollectionView.register(UINib(nibName: "TopBarViewCell", bundle: nil),forCellWithReuseIdentifier: topCollectionId)
        topCollectionView.register(HeaderCollectionViewEmpty.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerEmptyId)
    }
    
    func setupNoDataView() {
        self.view.addSubview(noDataView)
        _ = noDataView.anchor(self.topView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 60)
        noDataView.addSubview(noDataImageView)
        noDataView.addSubview(noDataLabel)
        
        _ = noDataImageView.anchor(nil, left: noDataView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
        noDataImageView.centerYAnchor.constraint(equalTo: noDataView.centerYAnchor).isActive = true
        
        _ = noDataLabel.anchor(nil, left: noDataImageView.rightAnchor, bottom: nil, right: noDataView.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        noDataLabel.centerYAnchor.constraint(equalTo: noDataView.centerYAnchor).isActive = true
        noDataView.isHidden = true
    }
    
    func setupViews() {
        self.view.addSubview(topView)
        self.view.addSubview(tableView)
        
        _ = topView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        _ = tableView.anchor(topView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.topView.addSubview(topCollectionView)
        _ = topCollectionView.anchor(topView.topAnchor, left: topView.leftAnchor, bottom: topView.bottomAnchor, right: topView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        activityIndicator.startAnimating()
    }
    
    @objc func getDepartureInfo(recognizer: UITapGestureRecognizer) {
        if let moreInfo = recognizer.view as? UIImageView {
            let indexDeparture = self.departureFrom[moreInfo.tag]
            showMoreInfoMenu(moreInfo: indexDeparture)
        }
    }
    
    let blackView = UIView()
    var departureInfo: Departure!
    var departureTicket: Departure!
    
    func showMoreInfoMenu(moreInfo: Departure) {
        departureInfo = moreInfo
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.isUserInteractionEnabled = true
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            
            window.addSubview(blackView)
            window.addSubview(collectionView)
            
            let height: CGFloat = 180
            let y = window.frame.height - height
            collectionView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.collectionView.frame = CGRect(x: 0, y: y, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }, completion: nil)
        }
    }
    
    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.collectionView.frame = CGRect(x: 0, y: window.frame.height, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }
        })
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let topCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = UIColor.black
        return cv
    }()
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor.white
        return tv
    }()
    
    let topView: UIView = {
        let view = UIView()
        return view
    }()
    
    let noDataView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1
        view.layer.borderColor = PRIMARY_COLOR.cgColor
        return view
    }()
    
    let noDataImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "warning")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let noDataLabel: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize14
        label.text = "no_results".localized() + "no_results_hint".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    lazy var buttonNo: UIButton = {
        let button = UIButton()
        button.setTitle("no".localized().uppercased(), for: .normal)
        button.titleLabel?.font = UIHelper.defaultAppFontSize14
        button.setTitleColor(PRIMARY_COLOR, for: .normal)
        button.addTarget(self, action: #selector(closeDialog), for: .touchUpInside)
        return button
    }()
    
    lazy var buttonYes: UIButton = {
        let button = UIButton()
        button.setTitle("yes".localized().uppercased(), for: .normal)
        button.titleLabel?.font = UIHelper.defaultAppFontSize14
        button.setTitleColor(PRIMARY_COLOR, for: .normal)
        button.addTarget(self, action: #selector(confirmDialog), for: .touchUpInside)
        return button
    }()
    
    let messageLabelTitle: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize24
        label.text = "confirm_reservation_title".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize14
        label.text = "confirm_reservation_message".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    let seperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    var indexSelected = 0
    var isFirst = true
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 64, height: 155))
        popupView.backgroundColor = UIColor.white
        
        popupView.addSubview(messageLabelTitle)
        popupView.addSubview(messageLabel)
        popupView.addSubview(buttonNo)
        popupView.addSubview(buttonYes)
        popupView.addSubview(seperator)
        
        _ = messageLabelTitle.anchor(popupView.topAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 24, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = messageLabel.anchor(messageLabelTitle.bottomAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        _ = seperator.anchor(messageLabel.bottomAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        _ = buttonNo.anchor(seperator.bottomAnchor, left: nil, bottom: nil, right: popupView.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 50, heightConstant: 35)
        _ = buttonYes.anchor(seperator.bottomAnchor, left: nil, bottom: nil, right: buttonNo.leftAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 50, heightConstant: 35)
        
        return popupView
    }
    
    @objc func closeDialog() {
        dismissPopupView()
    }
    
    @objc func confirmDialog() {
        activityIndicator.startAnimating()
        let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
        let aplicationUser = AplicationUser(json: userDataJSON)
        let userId = aplicationUser.id ?? 0
        let guid = departureTicket.prodajeFindGUID ?? ""
        let posId = ticket.posSif ?? 0
        let ticketId = ticket.tiketBrj ?? 0
        let ticketGod = ticket.tiketGod ?? 0
        let faktId = ticket.faktBrj ?? 0
        let faktGod = ticket.faktGod ?? 0
        let reservationRequest = ReservationRequest(userId: userId, guid: guid, token: token, posId: posId, ticketId: ticketId, ticketYear: ticketGod, faktId: faktId, faktYear: faktGod)
        webAPIController.setTicketReservation(reservationData: reservationRequest)
    }
}

extension DepartureViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topCollectionView {
            return topDays.count
        } else {
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == topCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topCollectionId, for: indexPath) as! TopBarViewCell
            let index = self.topDays[indexPath.row]
            cell.updateMain(item: index)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellCollectionId, for: indexPath) as! SettingsLauncherCell
            switch (indexPath.row) {
            case 0:
                cell.cellImage.image = UIImage(named: "map")
                cell.cellLabel.text = "Map_title".localized()
            case 1:
                cell.cellImage.image = UIImage(named: "timer")
                cell.cellLabel.text = "Bus_timetable_title".localized()
                
            default:
                print("Error cell CV")
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == topCollectionView {
            if isFirst {
                self.topDays[indexSelected].isSelected = false
                let indexPathClear = IndexPath(row: self.indexSelected, section: 0)
                let cell = collectionView.cellForItem(at: indexPathClear) as? TopBarViewCell
                cell?.bottomIndicator.isHidden = true
                indexSelected = indexPath.row
                isFirst = false
            }
            
            let index = self.topDays[indexPath.row]
            self.activityIndicator.startAnimating()
            indexSelected = indexPath.row
            let dateTrip = index.date.toString(dateFormat: DATE_FORMAT)
            
            if UIHelper.compareDates(dateOne: dateTrip, dateTwo: requestTrip._dateReturn) {
                requestTrip._dateReturn = dateTrip
                isDateLarger = true
            }
            self.webAPIController.getDepartures(stationFrom: requestTrip._stationFrom, stationTo: requestTrip._stationTo, dateTrip: dateTrip, dateReturn: requestTrip._dateReturn, isReturn: requestTrip._isReturn, isReturnDateKnown: requestTrip._isReturnDateKnown)
        } else {
            self.handleDismiss()
            let backItem = UIBarButtonItem()
            switch (indexPath.row) {
            case 0:
                let mapVC = StationMapViewController()
                mapVC.isForRoute = true
                mapVC.prodajaID = self.departureInfo.prodajeFindGUID!
                backItem.title = "Map_title".localized()
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(mapVC, animated: true)
            case 1:
                let scheduleVC = RideScheduleViewController()
                backItem.title = "Bus_timetable_title".localized()
                navigationItem.backBarButtonItem = backItem
                scheduleVC.departure = self.departureInfo
                self.navigationController?.pushViewController(scheduleVC, animated: true)
                
            default:
                print("Error in didSelect More Menu")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == topCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as? TopBarViewCell
            cell?.bottomIndicator.isHidden = true
            
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == topCollectionView {
            return CGSize(width: topCollectionView.frame.width / 4, height: topCollectionView.frame.height)
        } else {
            return CGSize(width: collectionView.frame.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if collectionView == topCollectionView {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerEmptyId, for: indexPath) as! HeaderCollectionViewEmpty
            
            return header
        } else {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HeaderCollectionView
            
            return header
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == topCollectionView {
            return CGSize(width: 0, height: 0)
        } else {
            return CGSize(width: view.frame.width, height: 50)
        }
    }
}

extension DepartureViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DepartureTableViewCell
        let index = self.departureFrom[indexPath.row]
        cell.dotsImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.getDepartureInfo(recognizer:)))
        cell.dotsImage.tag = indexPath.row
        cell.dotsImage.addGestureRecognizer(tap)

        cell.updateName(item: index)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departureFrom.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isFromTicket {
            self.departureTicket = departureFrom[indexPath.row]
            let popupView = createPopupview()
            let popupConfig = STZPopupViewConfig()
            popupConfig.dismissTouchBackground = true
            popupConfig.blurEffectStyle = .light
            popupConfig.dismissAnimation = .slideOutToBottom
            presentPopupView(popupView, config: popupConfig)
        } else {
            if departureTo.count > 0 {
                let tripVc = ReturnViewController()
                tripVc.returnTrips = departureTo
                tripVc.prices = self.departureFrom[indexPath.row].prices
                tripVc.departures = departures
                tripVc.requestTrip = self.requestTrip
                tripVc.stationNameTo = stationNameTo
                tripVc.stationNameFrom = stationNameFrom
                tripVc.isDateLarger = isDateLarger
                let backItem = UIBarButtonItem()
                backItem.title = "\(stationNameFrom)-\(stationNameTo)"
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(tripVc, animated: true)
            } else {
                let storyB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let detailsVC = storyB.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsViewController
                detailsVC.departure = departureFrom[indexPath.row]
                detailsVC.stationFrom = stationNameFrom
                detailsVC.stationTo = stationNameTo
                detailsVC.isRoundTrip = requestTrip._isReturn
                let backItem = UIBarButtonItem()
                backItem.title = "information".localized()
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(detailsVC, animated: true)
            }
        }
    }
}

extension DepartureViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let departureResponse = result as? BaseDeparture {
            self.departures = departureResponse
            self.departureFrom.removeAll()
            self.departureTo.removeAll()
            self.departureTo = self.departures.allReturns
            self.departureTo.sort {$0.departureTime! < $1.departureTime!}
            self.departureFrom = self.departures.allDepartures
            self.departureFrom.sort {$0.departureTime! < $1.departureTime!}
            if departureFrom.count == 0 {
                noDataView.isHidden = false
            } else {
                noDataView.isHidden = true
            }
            self.tableView.reloadData()
            self.topCollectionView.scrollToItem(at: IndexPath(row: indexSelected, section: 0), at: .right, animated: true)
            self.activityIndicator.stopAnimating()
        }
        
        if let reservationResponse = result as? ReservationResponse {
            if reservationResponse.reserved! {
                activityIndicator.stopAnimating()
                self.dismissPopupView()
                self.view.makeToast("successful_reservation".localized(), duration: 2.3, position: .center)
            }
            
            if (reservationResponse.error?.contains(find: SQL_ERROR))! {
                activityIndicator.stopAnimating()
                self.dismissPopupView()
                self.view.makeToast("return_trip_before_trip_err".localized(), duration: 2.3, position: .bottom)
            }
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        print("DepartureVC Error: \(String(describing: error))")
        noDataView.isHidden = false
        self.activityIndicator.stopAnimating()
    }
}
