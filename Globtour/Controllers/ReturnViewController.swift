//
//  ReturnViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/21/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import Toast_Swift

class ReturnViewController: UIViewController {
    
    let cellId = "cell"
    let cellCollectionId = "cellId"
    let topCollectionId = "cellIdTop"
    let headerId = "headerId"
    let headerEmptyId = "headerEmptyId"
    
    var requestTrip: RequestModel!
    let webAPIController = WebApiController()
    var departures: BaseDeparture!
    var returnTrips: [Departure] = []
    var sevenDays: [Date] = []
    var topDays: [DayList] = []
    var prices: [PriceDetail] = []
    
    var stationNameFrom = ""
    var stationNameTo = ""
    var tempDateTrip = ""
    var isDateLarger = false
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    var currentDateString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webAPIController.delegate = self
        setupViews()
        setupProgresView()
        currentDateString = requestTrip._dateReturn
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: "DepartureTableViewCell", bundle: nil),forCellReuseIdentifier: cellId)
        setupCollectionViews()
        getNextSevenDays()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "departure_one".localized()
    }
    
    func getNextSevenDays() {
        let dateToCompare = currentDateString.toDate(dateFormat: DATE_FORMAT)
        let minDate = Calendar.current.date(byAdding: .day, value: -8, to: dateToCompare)
        let todayDate = Date()
        
        if !isDateLarger {
            if todayDate < dateToCompare {
                for i in 0...7 {
                    let tempDate = Calendar.current.date(byAdding: .day, value: i, to: minDate!)
                    let dateToCompareString = tempDate?.toString(dateFormat: DATE_FORMAT)
                    if dateToCompareString == currentDateString {
                        self.topDays.append(DayList(date: tempDate!, isSelected: true))
                        indexSelected = i
                    } else {
                        self.topDays.append(DayList(date: tempDate!, isSelected: false))
                    }
                }
            }
        }
        
        if todayDate < dateToCompare {
            for item in topDays {
                let order = Calendar.current.compare(item.date, to: todayDate, toGranularity: .day)
                switch order {
                case .orderedAscending:
                    topDays = topDays.filter { $0 != item }
                case .orderedDescending:
                    print("drugo")
                case .orderedSame:
                    print("same")
                }
            }
        }
        for i in 0...8 {
            let tempDate = Calendar.current.date(byAdding: .day, value: i, to: dateToCompare)
            let dateToCompareString = tempDate?.toString(dateFormat: DATE_FORMAT)
            if dateToCompareString == currentDateString {
                self.topDays.append(DayList(date: tempDate!, isSelected: true))
                indexSelected = i
            } else {
                self.topDays.append(DayList(date: tempDate!, isSelected: false))
            }
        }
        var index = 0
        for i in topDays {
            if i.isSelected {
                indexSelected = index
            }
            index = index + 1
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.topCollectionView.scrollToItem(at: IndexPath(row: indexSelected, section: 0), at: .right, animated: true)
    }
    
    func setupNoDataView() {
        self.view.addSubview(noDataView)
        _ = noDataView.anchor(self.topView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 60)
        noDataView.addSubview(noDataImageView)
        noDataView.addSubview(noDataLabel)
        
        _ = noDataImageView.anchor(nil, left: noDataView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
        noDataImageView.centerYAnchor.constraint(equalTo: noDataView.centerYAnchor).isActive = true
        
        _ = noDataLabel.anchor(nil, left: noDataImageView.rightAnchor, bottom: nil, right: noDataView.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        noDataLabel.centerYAnchor.constraint(equalTo: noDataView.centerYAnchor).isActive = true
    }
    
    func setupCollectionViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsSelection = true
        collectionView.register(UINib(nibName: "SettingsLauncherCell", bundle: nil),forCellWithReuseIdentifier: cellCollectionId)
        collectionView.register(HeaderCollectionView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        
        topCollectionView.dataSource = self
        topCollectionView.delegate = self
        topCollectionView.allowsSelection = true
        topCollectionView.register(UINib(nibName: "TopBarViewCell", bundle: nil),forCellWithReuseIdentifier: topCollectionId)
        topCollectionView.register(HeaderCollectionViewEmpty.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerEmptyId)
    }
    
    func setupViews() {
        self.view.addSubview(topView)
        self.view.addSubview(tableView)
        
        _ = topView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        _ = tableView.anchor(topView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.topView.addSubview(topCollectionView)
        _ = topCollectionView.anchor(topView.topAnchor, left: topView.leftAnchor, bottom: topView.bottomAnchor, right: topView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        activityIndicator.startAnimating()
    }
    
    @objc func getDepartureInfo(recognizer: UITapGestureRecognizer) {
        if let moreInfo = recognizer.view as? UIImageView {
            let indexDeparture = self.returnTrips[moreInfo.tag]
            showMoreInfoMenu(moreInfo: indexDeparture)
        }
    }
    
    let blackView = UIView()
    var departureInfo: Departure!
    
    func showMoreInfoMenu(moreInfo: Departure) {
        departureInfo = moreInfo
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.isUserInteractionEnabled = true
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            
            window.addSubview(blackView)
            window.addSubview(collectionView)
            
            let height: CGFloat = 180
            let y = window.frame.height - height
            collectionView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.collectionView.frame = CGRect(x: 0, y: y, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }, completion: nil)
        }
    }
    
    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.collectionView.frame = CGRect(x: 0, y: window.frame.height, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }
        })
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let topCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.black
        return cv
    }()
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor.white
        return tv
    }()
    
    let topView: UIView = {
        let view = UIView()
        return view
    }()
    
    let noDataView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1
        view.layer.borderColor = PRIMARY_COLOR.cgColor
        return view
    }()
    
    let noDataImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "warning")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let noDataLabel: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize14
        label.text = "no_results".localized() + "no_results_hint".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    var indexSelected = 0
    var isFirst = true
}

extension ReturnViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topCollectionView {
            return self.topDays.count
        } else {
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == topCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topCollectionId, for: indexPath) as! TopBarViewCell
            let index = self.topDays[indexPath.row]
            cell.updateMain(item: index)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellCollectionId, for: indexPath) as! SettingsLauncherCell
            switch (indexPath.row) {
            case 0:
                cell.cellImage.image = UIImage(named: "map")
                cell.cellLabel.text = "Map_title".localized()
            case 1:
                cell.cellImage.image = UIImage(named: "timer")
                cell.cellLabel.text = "Bus_timetable_title".localized()
                
            default:
                print("Error cell CV")
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == topCollectionView {
            if isFirst {
                self.topDays[indexSelected].isSelected = false
                let indexPathClear = IndexPath(row: self.indexSelected, section: 0)
                let cell = collectionView.cellForItem(at: indexPathClear) as? TopBarViewCell
                cell?.bottomIndicator.isHidden = true
                indexSelected = indexPath.row
                isFirst = false
            }
            let index = self.topDays[indexPath.row]
            indexSelected = indexPath.row
            let tempDate = requestTrip._dateReturn.toDate(dateFormat: DATE_FORMAT)
            if index.date < tempDate {
                self.navigationController?.view.makeToast("Wrong Date", duration: 0.5, position: .bottom)
            } else {
                self.activityIndicator.startAnimating()
                let dateReturn = index.date.toString(dateFormat: DATE_FORMAT)
                self.webAPIController.getDepartures(stationFrom: requestTrip._stationFrom, stationTo: requestTrip._stationTo, dateTrip: requestTrip._dateTrip, dateReturn: dateReturn, isReturn: requestTrip._isReturn, isReturnDateKnown: requestTrip._isReturnDateKnown)
            }
        } else {
            self.handleDismiss()
            let backItem = UIBarButtonItem()
            switch (indexPath.row) {
            case 0:
                let mapVC = StationMapViewController()
                mapVC.isForRoute = true
                mapVC.prodajaID = self.departureInfo.prodajeFindGUID!
                backItem.title = "Map_title".localized()
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(mapVC, animated: true)
            case 1:
                let scheduleVC = RideScheduleViewController()
                backItem.title = "Bus_timetable_title".localized()
                navigationItem.backBarButtonItem = backItem
                scheduleVC.departure = self.departureInfo
                self.navigationController?.pushViewController(scheduleVC, animated: true)
                
            default:
                print("Error in didSelect More Menu")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == topCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as? TopBarViewCell
            cell?.bottomIndicator.isHidden = true
            
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == topCollectionView {
            return CGSize(width: topCollectionView.frame.width / 4, height: topCollectionView.frame.height)
        } else {
            return CGSize(width: collectionView.frame.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if collectionView == topCollectionView {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerEmptyId, for: indexPath) as! HeaderCollectionViewEmpty
            
            return header
        } else {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HeaderCollectionView
            
            return header
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == topCollectionView {
            return CGSize(width: 0, height: 0)
        } else {
            return CGSize(width: view.frame.width, height: 50)
        }
    }
}

extension ReturnViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DepartureTableViewCell
        let index = self.returnTrips[indexPath.row]
        cell.dotsImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.getDepartureInfo(recognizer:)))
        cell.dotsImage.tag = indexPath.row
        cell.dotsImage.addGestureRecognizer(tap)
        
        cell.updateName(item: index)
        cell.price.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return returnTrips.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailsVC = storyB.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsViewController
        detailsVC.departure = returnTrips[indexPath.row]
        detailsVC.stationFrom = stationNameFrom
        detailsVC.stationTo = stationNameTo
        detailsVC.ticketPriceTemp = prices
        detailsVC.isRoundTrip = requestTrip._isReturn
        let backItem = UIBarButtonItem()
        backItem.title = "information".localized()
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if returnTrips.count > 0 {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows, let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                // stop animating spinner after tableview is populated
                activityIndicator.stopAnimating()
            }
        }
    }
}

extension ReturnViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        self.departures = result as! BaseDeparture
        self.returnTrips.removeAll()
        self.returnTrips = self.departures.allReturns
        if returnTrips.count == 0 {
            noDataView.isHidden = false
        } else {
            noDataView.isHidden = true
        }
        self.tableView.reloadData()
        self.topCollectionView.scrollToItem(at: IndexPath(row: indexSelected, section: 0), at: .right, animated: true)
        self.activityIndicator.stopAnimating()
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        print("DepartureVC Error: \(String(describing: error))")
        self.activityIndicator.stopAnimating()
    }
}

