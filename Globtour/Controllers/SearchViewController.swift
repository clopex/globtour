//
//  SearchViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/7/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//


// basic usage
//self.view.makeToast("This is a piece of toast")

// toast with a specific duration and position
//self.view.makeToast("This is a piece of toast", duration: 3.0, position: .top)

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import STZPopupView

struct Coordinate {
    var latitude: String
    var longitude: String
}

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var fromLbl: UILabel!
    @IBOutlet weak var toLbl: UILabel!
    @IBOutlet weak var passangersLbl: UILabel!
    @IBOutlet weak var dateDepartureLbl: UILabel!
    @IBOutlet weak var returnLbl: UILabel!
    @IBOutlet weak var arrows: UIImageView!
    @IBOutlet weak var datePickerTextField: UITextField!
    @IBOutlet weak var returnPickerTextField: UITextField!
    @IBOutlet weak var toDestinationBtn: UIButton!
    @IBOutlet weak var fromDestinationBtn: UIButton!
    
    var dateStringFrom = ""
    var dateStringTo = ""
    var stationNumberFrom = 0
    var stationNumberTo = 0
    var stationFromName = ""
    var stationToName = ""
    var token = ""
    var stationIdCheck = 0
    
    let webAPIController = WebApiController()
    let datePicker = UIDatePicker()
    var returnPicker = UIPickerView()
    
    var isReturn = false
    var isReturndateKnown = false
    var firstUse = false
    var isDatePicked = false
    
    // Create a DatePicker
    let datePickerReturn: UIDatePicker = UIDatePicker()
    
    let returnData = ["DepartureOption_1".localized(), "DepartureOption_2".localized(), "DepartureOption_3".localized()]

    override func viewDidLoad() {
        super.viewDidLoad()
        //webAPIController.getMinimumBuildVersion()
        if !Reachability.isConnectedToNetwork() {
            if !firstUse {
                let popupView = createPopupview()
                let popupConfig = STZPopupViewConfig()
                popupConfig.dismissTouchBackground = false
                popupConfig.blurEffectStyle = .light
                popupConfig.dismissAnimation = .slideOutToBottom
                presentPopupView(popupView, config: popupConfig)
                firstUse = true
            }
        } else {
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissReturnPicker))
            self.view.addGestureRecognizer(tap)
            dismissPopupView()
            setupNavigationImageLogo()
            addTapGestureToImg()
            createDatePicker()
            datePickerTextField.delegate = self
            returnPickerTextField.delegate = self
            returnPickerTextField.inputView = returnPicker
            returnPicker.delegate = self
            webAPIController.delegate = self
            getToken()
            firstUse = false
        }
    }
    
    @objc func dismissReturnPicker() {
        datePickerReturn.removeFromSuperview()
    }
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 64, height: 155))
        popupView.backgroundColor = UIColor.white
        
        popupView.addSubview(messageLabelTitle)
        popupView.addSubview(messageLabel)
        popupView.addSubview(buttonCheck)
        
        _ = messageLabelTitle.anchor(popupView.topAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = messageLabel.anchor(messageLabelTitle.bottomAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        _ = buttonCheck.anchor(messageLabel.bottomAnchor, left: nil, bottom: nil, right: popupView.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 35)
        
        return popupView
    }
    
    @objc func tryAgainNetwork() {
        viewDidLoad()
    }
    
    lazy var buttonCheck: UIButton = {
        let button = UIButton()
        button.setTitle("try_again".localized(), for: .normal)
        button.titleLabel?.font = UIHelper.defaultAppFontSize14
        button.setTitleColor(PRIMARY_COLOR, for: .normal)
        button.addTarget(self, action: #selector(tryAgainNetwork), for: .touchUpInside)
        return button
    }()
    
    let messageLabelTitle: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize24
        label.text = "no_internet".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize14
        label.text = "err_connection".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    @objc func touchClose() {
        dismissPopupView()
    }
    
    private func getToken() {
        webAPIController.getToken()
        webAPIController.getCurrencies()
    }

    private func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.tintColor = PRIMARY_COLOR
        datePicker.datePickerMode = .date
        let todayDate = Date()
        datePicker.minimumDate = todayDate
        dateStringFrom = todayDate.toString(dateFormat: DATE_FORMAT)
        datePickerTextField.text = todayDate.toString(dateFormat: DATE_FORMAT_VISUAL)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissPicker))
        toolbar.setItems([doneButton], animated: false)
        datePickerTextField.inputAccessoryView = toolbar
        returnPickerTextField.inputAccessoryView = toolbar
        
        // asign the date picker to the textFields
        datePickerTextField.inputView = datePicker
    }
    
    @objc func dismissPicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        let dateFormatedToString = datePicker.date.toString(dateFormat: DATE_FORMAT_VISUAL)
        datePickerTextField.text = dateFormatedToString
        dateStringFrom = datePicker.date.toString(dateFormat: DATE_FORMAT)
        view.endEditing(true)
        if isDatePicked {
            self.view.frame.origin.y += 40
        }
    }
    
    private func addTapGestureToImg() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(rotateAndChange))
        arrows.isUserInteractionEnabled = true
        arrows.addGestureRecognizer(tap)
    }
    
    var isRotate = true
    @objc private func rotateAndChange() {
        UIView.animate(withDuration: 0.5) {
            self.arrows.transform = CGAffineTransform(rotationAngle: .pi)
        }
        self.arrows.transform = CGAffineTransform.identity
        let fromDestinationText = fromDestinationBtn.titleLabel?.text
        let toDestination = toDestinationBtn.titleLabel?.text
        
        self.fromDestinationBtn.setTitle(toDestination, for: .normal)
        self.toDestinationBtn.setTitle(fromDestinationText, for: .normal)
        let tempTo = stationNumberTo
        let tempFrom = stationNumberFrom
        let tempToN = stationToName
        let tempFromN = stationFromName
        stationFromName = tempToN
        stationToName = tempFromN
        stationNumberFrom = tempTo
        stationNumberTo = tempFrom
    }

    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        let dateFormatedToString = datePickerReturn.date.toString(dateFormat: DATE_FORMAT_VISUAL)
        returnPickerTextField.text = dateFormatedToString
        self.isReturndateKnown = true
        self.isReturn = true
        dateStringTo = datePickerReturn.date.toString(dateFormat: DATE_FORMAT)
        view.endEditing(true)
    }

    @IBAction func fromDestination(_ sender: Any) {
        let stationsViewController = self.storyboard?.instantiateViewController(withIdentifier: "StationsViewController") as! StationsViewController
        stationsViewController.delegate = self
        stationsViewController.isFromSearch = true
        stationsViewController.stationId = stationIdCheck
        isFrom = true
        self.navigationController?.pushViewController(stationsViewController, animated: true)
    }
    
    var isFrom = false
    
    @IBAction func toDestination(_ sender: Any) {
        
        let stationsViewController = self.storyboard?.instantiateViewController(withIdentifier: "StationsViewController") as! StationsViewController
        stationsViewController.delegate = self
        stationsViewController.isFromSearch = true
        stationsViewController.stationId = stationIdCheck
        isFrom = false
        self.navigationController?.pushViewController(stationsViewController, animated: true)
    }
    
    var numberOfPassangers = 1
    
    @IBAction func searchDepartures(_ sender: Any) {
        //webAPIController.sendEmail()
        if (stationNumberTo == 0 ) {
            let title = "Error!"
            let msg = "Morate Odabrati krajnje odrediste!"
            showAlert(title: title, message: msg)
        } else if (stationNumberFrom == 0) {
            let title = "Error!"
            let msg = "Morate Odabrati pocetno odrediste!"
            showAlert(title: title, message: msg)
        } else {
            let langString = Locale.current.languageCode
            let currencyId = UserDefaults.standard.getCurrencyId()
            let requestTrip = RequestModel(lang: langString!, stationFrom: stationNumberFrom, stationTo: stationNumberTo, dateTrip: dateStringFrom, dateReturn: dateStringTo, isReturn: isReturn, isReturnDateKnown: isReturndateKnown, currency: currencyId, token: token)
            let departureViewController = DepartureViewController()
            departureViewController.requestTrip = requestTrip
            departureViewController.stationNameTo = stationToName
            departureViewController.stationNameFrom = stationFromName
            let backItem = UIBarButtonItem()
            backItem.title = "\(stationFromName)-\(stationToName)"
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(departureViewController, animated: true)
        }
    }
    
    @IBAction func addPassanger(_ sender: Any) {
        numberOfPassangers = numberOfPassangers + 1
        if numberOfPassangers % 10 != 1 || numberOfPassangers == 11 {
            passangersLbl.text = "\(numberOfPassangers) " + "more_passanger".localized()
        } else {
            passangersLbl.text = "\(numberOfPassangers) " + "one_passanger".localized()
        }
    }
    
    @IBAction func removePassanger(_ sender: Any) {
        if numberOfPassangers != 1 {
            numberOfPassangers = numberOfPassangers - 1
            if numberOfPassangers % 10 != 1 || numberOfPassangers == 11 {
                passangersLbl.text = "\(numberOfPassangers) " + "more_passanger".localized()
            } else {
                passangersLbl.text = "\(numberOfPassangers) "  + "one_passanger".localized()
            }
        }
    }
}

extension SearchViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return returnData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return returnData[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 1 {
            isDatePicked = true
            datePickerReturn.datePickerMode = .date
            //let todayDate = Date()
            datePickerReturn.minimumDate = datePicker.date //todayDate
            self.view.addSubview(datePickerReturn)
            _ = datePickerReturn.anchor(nil, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            // Set some of UIDatePicker properties
            datePickerReturn.timeZone = NSTimeZone.local
            datePickerReturn.backgroundColor = UIColor.white

            // Add an event to call onDidChangeDate function when value is changed.
            datePickerReturn.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: .valueChanged)

            // Add DataPicker to the view
            self.view.addSubview(datePickerReturn)

            self.view.endEditing(true)
        } else if row == 0 {
            isDatePicked = false
            isReturn = false
            isReturndateKnown = false
            dateStringTo = ""
        } else if row == 2 {
            isDatePicked = false
            self.isReturn = true
            self.isReturndateKnown = false
            dateStringTo = ""
        }
        returnPickerTextField.text = returnData[row]
    }
}

extension SearchViewController: StationDelegate {
    func pickerDidFinishWithResult(sender: StationsViewController, pickerItems: Station) {
        if self.isFrom {
            self.fromDestinationBtn.setTitle(pickerItems.name, for: .normal)
            if let id = pickerItems.id {
                stationIdCheck = id
                stationNumberFrom = id
                stationFromName = pickerItems.name!
            }
        } else {
            if let id = pickerItems.id {
                stationIdCheck = id
                stationNumberTo = id
                stationToName = pickerItems.name!
            }
            self.toDestinationBtn.setTitle(pickerItems.name, for: .normal)
        }
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.isDatePicked {
            self.view.frame.origin.y -= 30
        }

        return true
    }
}

extension SearchViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let result = result as? Token {
            if result.token != nil {
                token = result.token
                let loginSessionRaw = NSKeyedArchiver.archivedData(withRootObject: result)
                UserDefaults.standard.set(loginSessionRaw, forKey: "login_session")
                DispatchQueue.global().async {
                    UserDefaults.standard.synchronize()
                }
            }
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        let errorTitle = "Error"
        let message = error?.localizedDescription ?? "error_unknown"
        showAlert(title: errorTitle, message: message)
    }
}





