//
//  DetailsViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/22/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SwiftyJSON
import CountryPicker
import Toast_Swift

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var routeLbl: UILabel!
    @IBOutlet weak var departureTimeLbl: UILabel!
    @IBOutlet weak var arrivalTimeLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var travelDurationLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    @IBOutlet weak var groupTicketSwitch: UISwitch!
    @IBOutlet weak var r1ReciveSwitch: UISwitch!
    @IBOutlet weak var termsSwitch: UISwitch!
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var nameSurnameField: UITextField!
    @IBOutlet weak var companyNameField: UITextField!
    @IBOutlet weak var companyAddressField: UITextField!
    @IBOutlet weak var companyOIBField: UITextField!
    
    
    @IBOutlet weak var wrongEmailImg: UIImageView!
    @IBOutlet weak var wrongPhoneImg: UIImageView!
    @IBOutlet weak var countryImg: UIImageView!
    
    @IBOutlet weak var r1StackView: UIStackView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var addPassengerView: UIView!
    @IBOutlet weak var passengerView: UIView!
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var payButton: UIButton!
    
    @IBOutlet weak var customerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var passangerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    
    let _dateFormat = "yyyy.MM.dd'T'HH:mm:ss"
    
    var customerHeightDefault: CGFloat!
    var scrollHeightDefault: CGFloat!
    var priceHolder: Double!
    var priceTotal: Double = 0.0
    var stationFrom = ""
    var stationTo = ""
    
    let pickerView = UIPickerView()
    
    var passangers = [Passanger]()
    var departure: Departure!
    var ticketPriceAndType = [PriceDetail]()
    var ticketPriceTemp = [PriceDetail]()
    var aplicationUser = AplicationUser()
    let webAPIController = WebApiController()
    
    var alertVC = UIAlertController()
    
    var isLogdIn = false
    var isRoundTrip = false
    var isNewUser = false
    var categoryId = 0
    var userID = 0
    var posID = 0
    var categoryName = ""
    let currencyName = UserDefaults.standard.getCurrencyName()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        tableView.allowsSelection = false
        setupDelegate()
        webAPIController.delegate = self
        getPricesAndInfo()
        pickerView.delegate = self
        countryPickerView.countryPickerDelegate = self
        customerHeightDefault = customerHeightConstraint.constant
        scrollHeightDefault = scrollViewHeightConstraint.constant
        setupBgViews()
        setupTapGesture()
        setupPickerView()
        fillUpTopView()
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150 // Move view 150 points upward
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 20 // Move view to original position
    }
    
    func setupDelegate() {
        nameSurnameField.delegate = self
        companyAddressField.delegate = self
        companyOIBField.delegate = self
        companyNameField.delegate = self
    }
    
    var token: String {
        return Token.shared?.token ?? ""
    }
    
    func fillUpTopView() {
        routeLbl.text = "\(stationFrom) - \(stationTo)"
        guard let comapanyName = departure.transportCompanyName else {return}
        companyLbl.text = comapanyName
        guard let duration = departure.duration else {return}
        travelDurationLbl.text = duration
        let dateStart = UIHelper.getDayAndMonth(stringDate: departure.departureTime!)
        let dateEnd = UIHelper.getDayAndMonth(stringDate: departure.arrivalTime!)
        let timeStart = UIHelper.getHourAndTime(stringDate: departure.departureTime!)
        let timeEnd = UIHelper.getHourAndTime(stringDate: departure.arrivalTime!)
        departureTimeLbl.text = "\(dateStart). | \(timeStart)"
        arrivalTimeLbl.text = "\(dateEnd). | \(timeEnd)"
        
        let imageData = UserDefaults.standard.getCountryFlag()
        let dataDecode:NSData = NSData(base64Encoded: imageData, options:.ignoreUnknownCharacters)!
        let avatarImage:UIImage = UIImage(data: dataDecode as Data)!
        countryImg.image = avatarImage
    }
    
    func setupTapGesture() {
        r1ReciveSwitch.addTarget(self, action: #selector(giveR1Recive), for: .valueChanged)
        termsSwitch.addTarget(self, action: #selector(checkForTerms), for: .valueChanged)
        let tapPassanger = UITapGestureRecognizer(target: self, action: #selector(setupAddPassengerBtn))
        addPassengerView.addGestureRecognizer(tapPassanger)
        let tapCountry = UITapGestureRecognizer(target: self, action: #selector(choseCountryPhone))
        countryImg.addGestureRecognizer(tapCountry)
    }
    
    func getPricesAndInfo() {
        guard let langStr = Locale.current.languageCode else {return}
        if departure.prices.count > 0 {
            for item in departure.prices {
                if item.language == langStr {
                    guard let category = item.kategNaz else {return}
                    guard let price = item.cijena else {return}
                    guard let langauge = item.language else {return}
                    guard let kategSif = item.kategSif else {return}
                    ticketPriceAndType.append(PriceDetail(category: category, price: price, language: langauge, kategSif: kategSif))
                }
            }
        } else {
            for item in ticketPriceTemp {
                if item.language == langStr {
                    guard let category = item.kategNaz else {return}
                    guard let price = item.cijena else {return}
                    guard let langauge = item.language else {return}
                    guard let kategSif = item.kategSif else {return}
                    ticketPriceAndType.append(PriceDetail(category: category, price: price, language: langauge, kategSif: kategSif))
                }
            }
        }
        
        isLogdIn = UserDefaults.standard.isUserLogIn()
        if isLogdIn {
            let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
            aplicationUser = AplicationUser(json: userDataJSON)
            guard let email = aplicationUser.email else {return}
            guard let phoneNum = aplicationUser.phoneNumber else {return}
            guard let id = aplicationUser.id else {return}
            guard let posId = aplicationUser.posId else {return}
            self.userID = id
            self.posID = posId
            
            self.emailField.text = email
            self.phoneField.text = "\(phoneNum)"
        }
    }
    
    func setupPickerView() {
        self.navigationController?.view.addSubview(countryPickerView)
        //        self.view.addSubview(countryPickerView)
        _ = countryPickerView.anchor(nil, left: self.navigationController?.view.leftAnchor, bottom: self.navigationController?.view.bottomAnchor, right: self.navigationController?.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    @objc func choseCountryPhone() {
        countryPickerView.isHidden = false
    }
    
    @objc func setupAddPassengerBtn() {
        print(self.customerHeightConstraint.constant)
        alertVC = UIAlertController(title: "passenger_info".localized(), message: nil, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "close".localized(), style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: "passengers".localized(), style: .default, handler: { alert -> Void in
            let name = self.alertVC.textFields![0] as UITextField
            let surname = self.alertVC.textFields![1] as UITextField
            let ticketType = self.alertVC.textFields![2] as UITextField
            
            if name.text == "" || surname.text == "" || ticketType.text == "" {
                let errorAlert = UIAlertController(title: "Error", message: "Please input all fields", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                    alert -> Void in
                    self.present(self.alertVC, animated: true, completion: nil)
                }))
                self.present(errorAlert, animated: true, completion: nil)
            } else {
//                let fullName = "\(name.text!) \(surname.text!)"
                self.passangers.append(Passanger(name: name.text!, surname: surname.text!, rowNum: 1, phone: "", price: self.priceHolder, categoryId: self.categoryId, categoryName: self.categoryName, ticketType: ticketType.text!))
                self.tableView.reloadData()
                self.priceTotal = self.priceTotal + self.priceHolder
                
                self.totalPriceLbl.text = "\(self.priceTotal) \(self.currencyName)"
                if self.passangers.count > 2 {
                    self.passangerViewHeightConstraint.constant = self.passangerViewHeightConstraint.constant + CGFloat(44)
                    self.scrollViewHeightConstraint.constant = self.scrollViewHeightConstraint.constant + CGFloat(44)
                }
            }
            
        }))
        
        alertVC.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "name".localized()
        })
        
        alertVC.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "surname".localized()
        })
        
        alertVC.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Tip karte"
            textField.tintColor = UIColor.clear
            textField.inputView = self.pickerView
            textField.delegate = self
        })
        
        
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func setupBgViews() {
        topView.dropShadow(color: UIColor.lightGray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 2, scale: true)
        passengerView.dropShadow(color: UIColor.lightGray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 2, scale: true)
        customerView.dropShadow(color: UIColor.lightGray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 2, scale: true)
        customerHeightConstraint.constant = self.customerHeightConstraint.constant - self.r1StackView.frame.height
        scrollViewHeightConstraint.constant = scrollViewHeightConstraint.constant - self.r1StackView.frame.height
        r1StackView.isHidden = true
        addPassengerView.createCircle()
    }

    @objc func giveR1Recive() {
        if r1ReciveSwitch.isOn {
            animateView(isOpen: false)
        } else {
            animateView(isOpen: true)
        }
    }
    
    @objc func checkForTerms() {
        if termsSwitch.isOn {
            payButton.isEnabled = true
        } else {
            payButton.isEnabled = false
        }
    }
    
    func animateView(isOpen: Bool) {
        if isOpen {
            UIView.animate(withDuration: 1.0, animations: {
                self.customerHeightConstraint.constant = self.customerHeightConstraint.constant - self.r1StackView.frame.height
                self.scrollViewHeightConstraint.constant = self.scrollViewHeightConstraint.constant - self.r1StackView.frame.height
                self.r1StackView.isHidden = true
            })
        } else {
            self.customerHeightConstraint.constant = customerHeightDefault
            self.r1StackView.isHidden = false
            self.scrollViewHeightConstraint.constant = self.scrollHeightDefault
        }
    }
    
    @IBAction func confirmBtn(_ sender: Any) {
        
        if passangers.count == 0 {
            let title = "Error"
            let msg = "passengers_err".localized()
            showAlert(title: title, message: msg)
        } else if isLogdIn {
            if validate() {
                getDataForBasket()
            }
        } else {
            if validate() {
                let emailField = self.emailField.text ?? ""
                self.webAPIController.getUserByName(email: emailField)
            }
        }
    }
    
    func getDataForBasket() {
        let storyB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let basketVC = storyB.instantiateViewController(withIdentifier: "BasketVC") as! BasketViewController
        basketVC.passangers = passangers
        basketVC.stationFrom = stationFrom
        basketVC.stationTo = stationTo
        basketVC.departure = departure
        basketVC.totalPrice = priceTotal
        basketVC.ticketCategory = ticketPriceAndType
        basketVC.userId = self.userID
        basketVC.isRoundTrip = self.isRoundTrip
        if r1ReciveSwitch.isOn {
            let nameSurname = nameSurnameField.text ?? ""
            let companyName = companyNameField.text ?? ""
            let companyAddress = companyAddressField.text ?? ""
            let companyOib = companyOIBField.text ?? ""
            let r1Recive = R1recive(nameSurname: nameSurname, companyName: companyName, companyAddress: companyAddress, companyOib: companyOib)
            basketVC.r1Receive = r1Recive
            basketVC.isR1ReciveSelected = true
        }
        
        if groupTicketSwitch.isOn {
            basketVC.isGroupTicket = true
        }
        
        let backItem = UIBarButtonItem()
        backItem.title = "cart".localized()
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(basketVC, animated: true)
    }
    
    func validate() -> Bool {
        var valid = true
        
        let email = emailField.text!
        let phone = phoneField.text!
        
        if email.isEmpty {
            wrongEmailImg.isHidden = false
            valid = false
        } else {
            if !UIHelper.checkEmail(email: email) {
                wrongEmailImg.isHidden = false
                valid = false
            } else {
                wrongEmailImg.isHidden = true
            }
        }
        
        if phone.isEmpty {
            wrongPhoneImg.isHidden = false
            valid = false
        } else if phone.count < 12 {
            wrongPhoneImg.isHidden = false
            let title = "Error"
            let msg = "phone_error".localized()
            showAlert(title: title, message: msg)
        } else {
            if !UIHelper.validatePhone(value: phone) {
                wrongPhoneImg.isHidden = false
                valid = false
            } else {
                wrongPhoneImg.isHidden = true
            }
        }
        
        if r1ReciveSwitch.isOn {
            let address = companyAddressField.text!
            let nameSurname = nameSurnameField.text!
            let oib = companyOIBField.text!
            let name = companyNameField.text!
            
            if name.isEmpty {
                createWrongSign(textField: companyNameField)
                valid = false
            }
            
            if oib.isEmpty {
                createWrongSign(textField: companyOIBField)
                valid = false
            }
            
            if address.isEmpty {
                createWrongSign(textField: companyAddressField)
                valid = false
            }
            
            if nameSurname.isEmpty {
                createWrongSign(textField: nameSurnameField)
                valid = false
            }

        }
        return valid
    }
    
    func createWrongSign(textField: UITextField) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        let image = UIImage(named: "wrong")
        imageView.image = image
        imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(showToast))
        imageView.addGestureRecognizer(tap)
        textField.rightViewMode = UITextFieldViewMode.always
        textField.rightView = imageView
    }
    
    @objc func showToast() {
        self.navigationController?.view.makeToast("required_field".localized(), duration: 0.5, position: .bottom)
    }
    
    lazy var countryPickerView: CountryPicker = {
        let picker = CountryPicker()
        picker.showPhoneNumbers = true
        picker.backgroundColor = UIColor.white
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        picker.setCountry(code!)
        picker.isHidden = true
        return picker
    }()
    
}

extension DetailsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFiledInAletVC = alertVC.textFields![2] as UITextField
        if textField == textFiledInAletVC {
            self.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameSurnameField {
            nameSurnameField.rightView = nil
        }
        
        if textField == companyNameField {
            textField.rightView = nil
        }
        
        if textField == companyAddressField {
            textField.rightView = nil
        }
        
        if textField == companyOIBField {
            textField.rightView = nil
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let textFiledInAletVC = alertVC.textFields![2] as UITextField
        if textField == textFiledInAletVC {
            textField.text = ticketPriceAndType[0].kategNaz
            self.categoryName = ticketPriceAndType[0].kategNaz!
            self.categoryId = ticketPriceAndType[0].kategSif!
            self.priceHolder = ticketPriceAndType[0].cijena
        }
        return true
    }
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passangers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! PassengerTableViewCell
        let index = passangers[indexPath.row]
        cell.updateData(item: index)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.priceTotal = self.priceTotal - self.passangers[indexPath.row].price!
            self.totalPriceLbl.text = "\(self.priceTotal) \(self.currencyName)"
            self.passangers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            if self.passangers.count > 2 {
                self.passangerViewHeightConstraint.constant = self.passangerViewHeightConstraint.constant - 44
                self.scrollViewHeightConstraint.constant = self.scrollViewHeightConstraint.constant - 44
            }
        }
    }
}

extension DetailsViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let userData = result as? String {
            UserDefaults.standard.setLogdIn(value: true)
            UserDefaults.standard.setUserData(value: userData)
            let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
            aplicationUser = AplicationUser(json: userDataJSON)
            self.userID = aplicationUser.id ?? 0
            self.getDataForBasket()
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        let err = error?.localizedDescription ?? ""
        if err == "Response could not be serialized, input data was nil or zero length." {
            let emailField = self.emailField.text ?? ""
            let userData = AplicationUser(_acceptTerms: true, _email: emailField, _emailConfirmed: false, _accessFailedCount: 0, _id: 0, _lockoutEnabled: false, _passwordHash: DEFAULT_PASSWORD, _phoneNumber: "", _phoneNumberConfirmed: false, _username: emailField, _name: "", _surname: "", _marketing: false, _token: token, _agencyId: 0, _posId: 0)
            self.webAPIController.registerUser(userData: userData)
        }
    }
}

extension DetailsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.ticketPriceAndType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ticketPriceAndType[row].kategNaz
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let textFiledInAletVC = alertVC.textFields![2] as UITextField
        textFiledInAletVC.text = ticketPriceAndType[row].kategNaz
        self.categoryName = ticketPriceAndType[row].kategNaz!
        self.categoryId = ticketPriceAndType[row].kategSif!
        self.priceHolder = ticketPriceAndType[row].cijena
    }
}

extension DetailsViewController: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.countryPickerView.isHidden = true
        self.countryImg.image = flag
        self.phoneField.text = phoneCode
    }
}
