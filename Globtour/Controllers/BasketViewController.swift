//
//  BasketViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/24/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SwiftyJSON
import CryptoSwift
import SwiftSoup
import STZPopupView
import Toast_Swift

struct ListTicket {
    var title: String
    var price: Double
}

class BasketViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var basketView: UIView!
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var dateTimeStartLbl: UILabel!
    @IBOutlet weak var dateTimeEndLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    @IBOutlet weak var creditCardField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var creditCardNumberField: UITextField!
    @IBOutlet weak var cardCVCField: UITextField!
    @IBOutlet weak var monthField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    
    @IBOutlet weak var wrongCardNumber: UIImageView!
    @IBOutlet weak var wrongCvc: UIImageView!
    @IBOutlet weak var wrongName: UIImageView!
    @IBOutlet weak var wrongSurname: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var payButton: UIButton!
    
    @IBOutlet weak var baskerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var departure: Departure!
    var r1Receive: R1recive!
    var getBasket: Basket!
    var basketRequest: BasketRequestModel!
    var passangers =  [Passanger]()
    var ticketList = [ListTicket]()
    var ticketCategory = [PriceDetail]()
    let webAPIController = WebApiController()
    
    let cardPickerView = UIPickerView()
    let monthPickerView = UIPickerView()
    let yearPickerView = UIPickerView()
    
    var stationFrom = ""
    var stationTo = ""
    var creditCardNamePicker = "AMEX"
    var totalPrice = 0.0
    var isRoundTrip = false
    var isR1ReciveSelected = false
    var isGroupTicket = false
    var userId = 0
    
    //Array for spinners
    let months = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    var years = [String]()
    let creditCards = ["American Express", "Mastercard", "Visa", "Diners Club", "Maestro", "Discover"]
    let creditCardsSort = ["AMEX", "MASTERCARD", "VISA", "DINERS", "MAESTRO", "DISCOVER"]
    let _dateFormat = "yyyy.MM.dd'T'HH:mm:ss"
    
    let currencyName = UserDefaults.standard.getCurrencyName()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        addObservers()
        fillYears()
        setupDelegate()
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        getBasketInfo()
        tableView.allowsSelection = false
        creditCardField.inputView = cardPickerView
        monthField.inputView = monthPickerView
        yearField.inputView = yearPickerView
        setupBgViews()
        fillUpTopView()
        totalPriceLbl.text = "\(totalPrice) \(self.currencyName)"
        getTicketList()
        monthField.text = months[0]
        yearField.text = years[0]
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notifcation) in
            self.keyboardWillShow(notification: notifcation)
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notifcation) in
            self.keyboardWillHide(notification: notifcation)
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo, let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height, right: 0)
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeObservers()
    }
    
    func fillYears() {
        let currentDate = Date()
        let calendar = Calendar.current
        var year = calendar.component(.year, from: currentDate)
        
        for _ in 0...9 {
            years.append("\(year)")
            year = year + 1
        }
    }
    
    func setupDelegate() {
        cardPickerView.delegate = self
        monthPickerView.delegate = self
        yearPickerView.delegate = self
        webAPIController.delegate = self
        creditCardNumberField.delegate = self
        cardCVCField.delegate = self
        nameField.delegate = self
        surnameField.delegate = self
    }
    
    var token: String {
        return Token.shared?.token ?? ""
    }
    
    func getBasketInfo() {
        let curId = UserDefaults.standard.getCurrencyId()
        var index = 1
        for item in passangers {
            item.rowNum = index
            index += 1
        }
        
        let basketRequestModel = BasketRequestModel(id: userId, guid: departure.prodajeFindGUID!, guidReturn: "", isRoundTrip: isRoundTrip, token: token, currencyId: curId, passengers: passangers)
        print(basketRequestModel.toJSON())
        webAPIController.getBasketInfo(data: basketRequestModel)
    }
    
    func getTicketList() {
        var tempIndex = 0
        var tempPrice = 0.0
        var name = ""
        for category in ticketCategory {
            for item in passangers {
                if category.kategNaz == item.ticketType {
                    tempIndex = tempIndex + 1
                    tempPrice = tempPrice + item.price!
                    name = item.ticketType!
                }
            }
            if tempIndex != 0 {
                ticketList.append(ListTicket(title: "\(tempIndex) - \(name)", price: tempPrice))
            }
            
            if self.ticketList.count > 2 {
                self.baskerHeightConstraint.constant = self.baskerHeightConstraint.constant + CGFloat(44)
                self.scrollHeightConstraint.constant = self.scrollHeightConstraint.constant + CGFloat(44)
            }
            tempIndex = 0
            tempPrice = 0.0
        }
    }
    
    func setupBgViews() {
        topView.dropShadow(color: UIColor.lightGray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 2, scale: true)
        basketView.dropShadow(color: UIColor.lightGray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 2, scale: true)
        cardView.dropShadow(color: UIColor.lightGray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 2, scale: true)
    }
    
    func fillUpTopView() {
        destinationLbl.text = "\(stationFrom) - \(stationTo)"
        guard let comapanyName = departure.transportCompanyName else {return}
        companyLbl.text = comapanyName
        guard let duration = departure.duration else {return}
        durationLbl.text = duration
        let dateStart = UIHelper.getDayAndMonth(stringDate: departure.departureTime!)
        let dateEnd = UIHelper.getDayAndMonth(stringDate: departure.arrivalTime!)
        let timeStart = UIHelper.getHourAndTime(stringDate: departure.departureTime!)
        let timeEnd = UIHelper.getHourAndTime(stringDate: departure.arrivalTime!)
        dateTimeStartLbl.text = "\(dateStart). | \(timeStart)"
        dateTimeEndLbl.text = "\(dateEnd). | \(timeEnd)"
    }
    
    func setToastForWrongSign() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(showToast))
        wrongName.addGestureRecognizer(tap)
        wrongSurname.addGestureRecognizer(tap)
        wrongCvc.addGestureRecognizer(tap)
    }
    
    @objc func showToast() {
        self.navigationController?.view.makeToast("required_field".localized(), duration: 0.5, position: .bottom)
    }
    
    func validate(cardNumber: String, cardCvc: String, name: String, surname: String) -> Bool {
        var valid = true
        
        if name.isEmpty {
            wrongName.isHidden = false
            valid = false
        } else {
            wrongName.isHidden = true
        }
        
        if surname.isEmpty {
            wrongSurname.isHidden = false
            valid = false
        } else {
            wrongSurname.isHidden = true
        }
        
        if cardCvc.isEmpty {
            wrongCvc.isHidden = false
            valid = false
        } else {
            wrongCvc.isHidden = true
        }
        
        if cardNumber.isEmpty {
            wrongCardNumber.isHidden = false
            valid = false
        } else {
            if !cardNumber.luhnCheck() {
                self.navigationController?.view.makeToast("invalid_card_err".localized(), duration: 1.0, position: .bottom)
                wrongCardNumber.isHidden = false
                valid = false
            } else {
                wrongCardNumber.isHidden = true
            }
        }
        return valid
    }
    
    var paymentCommitedInoviceRequest: PaymentCommitedInvoiceRequest!
    var customerEmail = ""
    var wsPaySendResponseToSend: WsPaySend!
    
    @IBAction func payButton(_ sender: Any) {
        let creditCardName = creditCardNamePicker
        let creditCardNumber = creditCardNumberField.text ?? ""
        let creditCardCVC = cardCVCField.text ?? ""
        let year = yearField.text ?? ""
        let month = monthField.text ?? ""
        let name = nameField.text ?? ""
        let surname = surnameField.text ?? ""
        let racTmpCode = getBasket.racTmpCode ?? 0
        let shopingCartId = getBasket.myBasketId ?? 0
        let todayDate = Date()
        let dateForPay = todayDate.toString(dateFormat: "yyyyMMddhhmmss")
        let amountToPayString = String(totalPrice)
        let amountToPay = amountToPayString.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
        let plainTextsignature = SHOP_ID + SECRET_KEY + "\(shopingCartId)" + SECRET_KEY + amountToPay + SECRET_KEY
        print("----- \(plainTextsignature) -----")
        //let plainTextSignatureToken = SHOP_ID_TOKEN + SECRET_KEY_TOKEN + "\(shopingCartId)" + SECRET_KEY_TOKEN + "\(totalPrice)" + SECRET_KEY_TOKEN
        let replaceName = replaceCharacters(myString: name)
        let replaceSurname = replaceCharacters(myString: surname)
        let yearSubString = year.suffix(2)
        let expirationDate = yearSubString + month
        let wsPaySignature = plainTextsignature.md5()
//        let wsPaySignatureToken = plainTextSignatureToken.md5()
        print("-----** \(wsPaySignature) **------")
        let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
        let aplicationUser = AplicationUser(json: userDataJSON)
        customerEmail = aplicationUser.email ?? ""
        let customerPhone = aplicationUser.phoneNumber ?? ""
        let userID = aplicationUser.id ?? 0
        let posID = aplicationUser.posId ?? 0
        let myBasketID = getBasket.myBasketId ?? 00

        if isR1ReciveSelected {
            let namesurname = r1Receive.nameSurname ?? ""
            let companyAddress = r1Receive.companyAddress ?? ""
            let companyOib = r1Receive.companyOib ?? ""
            let companyName = r1Receive.companyName ?? ""
            paymentCommitedInoviceRequest = PaymentCommitedInvoiceRequest(racTmpCode: racTmpCode, myBasketId: myBasketID, userId: userID, posId: posID, softwareVersion: "", token: token, faktR1: isR1ReciveSelected, isGroup: isGroupTicket, faktName: namesurname, faktAdr: companyAddress, faktCompany: companyName, faktOib: companyOib, approvalCode: "", cardNum: "")
        } else {
            paymentCommitedInoviceRequest = PaymentCommitedInvoiceRequest(racTmpCode: racTmpCode, myBasketId: myBasketID, userId: userID, posId: posID, softwareVersion: "", token: token, faktR1: isR1ReciveSelected, isGroup: isGroupTicket, faktName: "", faktAdr: "", faktCompany: "", faktOib: "", approvalCode: "", cardNum: "")
        }

        if validate(cardNumber: creditCardNumber, cardCvc: creditCardCVC, name: name, surname: surname) {
            self.activityIndicator.startAnimating()
            payButton.isEnabled = false
            scrollView.isScrollEnabled = false
            
            let wsPaySend = WsPaySend(paymenyPlan: "0000", totalAmount: amountToPay, dateTime: dateForPay, shopId: SHOP_ID, shoppingCartId: "\(shopingCartId)", signature: wsPaySignature, customerAddress: CUSTOMER_ADDRESS, customerCity: CUSTOMER_CITY, customerCountry: CUSTOMER_COUNTRY, customerZip: CUSTOMER_ZIP, customerEmail: customerEmail, customerPhone: customerPhone, token: token, userId: userID, racTmpCode: racTmpCode, myBasketId: myBasketID, isTokenRequest: true, creditCardName: creditCardName, creditCardNumber: creditCardNumber, cardVerificationData: creditCardCVC, expirationDate: String(expirationDate), customerFirstName: replaceName, customerLastName: replaceSurname)

            webAPIController.paymingGatewayTransaction(data: wsPaySend)
        }
    }
    
    func replaceCharacters(myString: String) -> String {
        var string = myString
        string = string.replacingOccurrences(of: "č", with: "c")
        string = string.replacingOccurrences(of: "ć", with: "c")
        string = string.replacingOccurrences(of: "š", with: "s")
        string = string.replacingOccurrences(of: "đ", with: "d")
        string = string.replacingOccurrences(of: "ž", with: "z")
        string = string.replacingOccurrences(of: "Č", with: "C")
        string = string.replacingOccurrences(of: "Ć", with: "C")
        string = string.replacingOccurrences(of: "Š", with: "S")
        string = string.replacingOccurrences(of: "Đ", with: "D")
        string = string.replacingOccurrences(of: "Ž", with: "Z")
        
        return string
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var successView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.borderWidth = 1
        return view
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIHelper.defaultAppFontSize14
        label.text = "dialog_successful_message".localized()
        label.numberOfLines = 0
        label.textColor = UIColor.darkGray
        return label
    }()
    
    let messageImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "checkbox")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    lazy var buttonMy: UIButton = {
        let button = UIButton()
        button.backgroundColor = PRIMARY_COLOR
        button.setTitleColor(.white, for: .normal)
        button.setTitle("my_tickets".localized(), for: .normal)
        button.clipsToBounds = true
        button.titleLabel?.font = UIHelper.defaultAppFont
        button.layer.cornerRadius = 1
        button.addTarget(self, action: #selector(openTickets), for: .touchUpInside)
        return button
    }()
    
    @objc func openTickets() {
        activityIndicator.stopAnimating()
        self.tabBarController?.selectedIndex = 1
        self.navigationController?.popToRootViewController(animated: true)
        dismissPopupView()
    }
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 64, height: 180))
        popupView.backgroundColor = UIColor.white
        
        popupView.addSubview(messageImage)
        popupView.addSubview(messageLabel)
        popupView.addSubview(buttonMy)
        
        _ = messageImage.anchor(popupView.topAnchor, left: popupView.leftAnchor, bottom: nil, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        _ = messageLabel.anchor(messageImage.topAnchor, left: messageImage.rightAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        _ = buttonMy.anchor(messageLabel.bottomAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 40)
        
        return popupView
    }
    
    @objc func touchClose() {
        dismissPopupView()
    }
}

extension BasketViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ticketList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! BasketTableViewCell
        
        let index = ticketList[indexPath.row]
        cell.updateData(item: index)
        return cell
    }
}

extension BasketViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cardPickerView {
            return 6
        } else if pickerView == monthPickerView {
            return 12
        } else {
            return 10
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cardPickerView {
            return self.creditCards[row]
        } else if pickerView == monthPickerView {
            return self.months[row]
        } else {
            return self.years[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == cardPickerView {
            self.creditCardField.text = self.creditCards[row]
            self.creditCardNamePicker = self.creditCardsSort[row]
        } else if pickerView == monthPickerView {
            self.monthField.text = self.months[row]
        } else {
            self.yearField.text = self.years[row]
        }
        self.view.endEditing(true)
    }
}

extension BasketViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let getBasketData = result as? Basket {
            self.activityIndicator.stopAnimating()
            self.getBasket = getBasketData
        }
        
        if let wsPaySendResponse = result as? WsPaySend {
            let shopID = wsPaySendResponse.shopId ?? ""
            let shoppingCardId = wsPaySendResponse.shoppingCartId ?? ""
            let totalAmount = wsPaySendResponse.totalAmount ?? ""
            let plainTextSignature = shopID + SECRET_KEY + shoppingCardId + SECRET_KEY + totalAmount + SECRET_KEY
            let wsPaySignature = plainTextSignature.md5()
            
            wsPaySendResponse.signature = wsPaySignature
            self.wsPaySendResponseToSend = wsPaySendResponse
            
            self.webAPIController.wsPaySendTransaction(paySend: wsPaySendResponse)
        }
        
        if let payTransactionString = result as? String {
            do{
                let doc: Document = try! SwiftSoup.parse(payTransactionString)
                
                let errorMessage: String = try! doc.select("input[name=ErrorMessage]").attr("value")
                
                let wsPayOrderId: String = try! doc.select("input[name=WsPayOrderId]").attr("value")
                //let shoppingCartID: String = try! doc.select("input[name=ShoppingCartID]").attr("value")
                let customerFirstName: String = try! doc.select("input[name=CustomerFirstName]").attr("value")
                let customerLastName: String = try! doc.select("input[name=CustomerLastName]").attr("value")
                let customerAddress: String = try! doc.select("input[name=CustomerAddress]").attr("value")
                let customerCity: String = try! doc.select("input[name=CustomerCity]").attr("value")
                let customerZIP: String = try! doc.select("input[name=CustomerZIP]").attr("value")
                let customerCountry: String = try! doc.select("input[name=CustomerCountry]").attr("value")
                let customerPhone: String = try! doc.select("input[name=CustomerPhone]").attr("value")
                let customerEmail: String = try! doc.select("input[name=CustomerEmail]").attr("value")
                let ECI: String = try! doc.select("input[name=ECI]").attr("value")
                let XID: String = try! doc.select("input[name=XID]").attr("value")
                let CAVV: String = try! doc.select("input[name=CAVV]").attr("value")
                let maskedPan: String = try! doc.select("input[name=MaskedPan]").attr("value")
                let creditCardName: String = try! doc.select("input[name=CreditCardName]").attr("value")
                let paymentPlan: String = try! doc.select("input[name=PaymentPlan]").attr("value")
                let approvalCode: String = try! doc.select("input[name=ApprovalCode]").attr("value")
                let approved: String = try! doc.select("input[name=Approved]").attr("value")
                let totalAmount: String = try! doc.select("input[name=TotalAmount]").attr("value")
                let signature: String = try! doc.select("input[name=Signature]").attr("value")
                let tokenDoc: String = try! doc.select("input[name=Token]").attr("value")
                
                let wsPayReceive = WsPayReceive(approvalCode: approvalCode, approved: approved, cavv: CAVV, creditCardName: creditCardName, customerAddress: customerAddress, customerCity: customerCity, customerCountry: customerCountry, customerEmail: customerEmail, customerFirstName: customerFirstName, customerLastName: customerLastName, customerPhone: customerPhone, customerZip: customerZIP, eci: ECI, errorMsg: errorMessage, isTokenRequest: false, maskedPlan: maskedPan, paymentPlan: paymentPlan, shoppingCardId: wsPaySendResponseToSend.shoppingCartId!, signature: signature, token: self.token, totalAmount: totalAmount, wsToken: tokenDoc, wsTokenNumber: "", wsPayOrderId: wsPayOrderId, xid: XID)
                
//                let approCode = wsPayReceive.approvalCode ?? ""
//                let cardNumeSend = wsPayReceive.maskedPlan ?? ""
                let postPayment = PostPayment(email: customerEmail, tokenNumber: tokenDoc, wsResponse: wsPayReceive, invoiceRequest: paymentCommitedInoviceRequest, wsPaySend: wsPaySendResponseToSend)
                
                self.webAPIController.updatePaymingGatewayTransaction(postPayment: postPayment)
                
            }catch Exception.Error(let message){
                print(message)
            }catch{
                print("error")
            }
        }
        
        if let finish = result as? Bool {
            if finish {
                let popupView = createPopupview()
                presentPopupView(popupView)
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        self.navigationController?.view.makeToast(error?.localizedDescription, duration: 1.0, position: .bottom)
    }
}

extension BasketViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameField {
            wrongName.isHidden = true
        }
        
        if textField == surnameField {
            wrongSurname.isHidden = true
        }
        
        if textField == cardCVCField {
            wrongCvc.isHidden = true
        }
        
        if textField == creditCardNumberField {
            wrongCardNumber.isHidden = true
        }
    }
}








