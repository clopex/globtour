//
//  CurrenciesViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/17/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class CurrenciesViewController: UITableViewController {

    let cellId = "cellId"
    
    var currencies = [CurrencyData]()
    
    let webAPIController = WebApiController()
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationImageLogo()
        webAPIController.delegate = self
        tableView.register(UINib(nibName: "CurrencyTableViewCell", bundle: nil),forCellReuseIdentifier: cellId)
        tableView.separatorInset = UIEdgeInsets.zero
        setupProgresView()
        activityIndicator.startAnimating()
        webAPIController.getCurrencies()
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CurrencyTableViewCell
        let index = self.currencies[indexPath.row]
        cell?.updateData(item: index)
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let name = currencies[indexPath.row]
        if let nameToSave = name.shortName {
            UserDefaults.standard.setCurrencyName(value: nameToSave)
        }
        
        if let id = name.id {
            UserDefaults.standard.setCurrencyId(value: id)
        }
        navigationController?.popToRootViewController(animated: true)
    }
}

extension CurrenciesViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let data = result as? [CurrencyData] {
            currencies = data
            tableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        let title = "Error"
        let msg = "Greska prilikom dohvacanja valuta!"
        activityIndicator.stopAnimating()
        showAlert(title: title, message: msg)
    }
}
