//
//  RideScheduleViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/16/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class RideScheduleViewController: UIViewController {
    
    var departure: Departure!
    let webAPIController = WebApiController()
    var itineraries = [Itinerary]()
    let cellId = "cellId"
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webAPIController.delegate = self
        setupNavigationImageLogo()
        setupProgresView()
        self.view.backgroundColor = UIColor.white
        tableView.delegate = self
        tableView.dataSource = self
        setupViews()
        tableView.register(UINib(nibName: "RideScheduleTableViewCell", bundle: nil),forCellReuseIdentifier: cellId)
        itineraries = departure.itineraries
        activityIndicator.startAnimating()
        webAPIController.getAllStations(id: 0)
        
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        activityIndicator.startAnimating()
    }
    
    private func setupNavigationImageLogo() {
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.contentMode = .scaleAspectFit
        titleView.addSubview(logoImage)
        
        _ = logoImage.anchor(titleView.topAnchor, left: titleView.leftAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.navigationItem.titleView = titleView
    }
    
    func setupViews() {
        self.view.addSubview(topView)
        topView.addSubview(stationLabel)
        topView.addSubview(arrivalLabel)
        topView.addSubview(departureLabel)
        self.view.addSubview(seperatorView)
        self.view.addSubview(tableView)
        
        _ = topView.anchor(self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        _ = stationLabel.anchor(topView.topAnchor, left: topView.leftAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = departureLabel.anchor(topView.topAnchor, left: nil, bottom: nil, right: topView.rightAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        _ = arrivalLabel.anchor(topView.topAnchor, left: nil, bottom: nil, right: self.departureLabel.leftAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 32, widthConstant: 0, heightConstant: 0)
        _ = seperatorView.anchor(topView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 1)

        _ = tableView.anchor(seperatorView.bottomAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
    }
    
    let stationLabel: UILabel = {
        let name = UILabel()
        name.text = "bus_station".localized()
        name.font = UIHelper.defaultAppFont
        name.textColor = UIColor.black
        return name
    }()
    
    let arrivalLabel: UILabel = {
        let name = UILabel()
        name.text = "bus_station_arrival".localized()
        name.font = UIHelper.defaultAppFont
        name.textColor = UIColor.black
        return name
    }()
    
    let departureLabel: UILabel = {
        let name = UILabel()
        name.text = "bus_station_departure".localized()
        name.font = UIHelper.defaultAppFont
        name.textColor = UIColor.black
        return name
    }()
    
    let topView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let seperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor.white
        return tv
    }()
}

extension RideScheduleViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        if let stations = result as? [Station] {
            allStations.removeAll()
            allStations = stations
            tableView.reloadData()
            activityIndicator.stopAnimating()
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        print("Fetch Stations Error: \(String(describing: error))")
        activityIndicator.stopAnimating()
    }
}

extension RideScheduleViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itineraries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RideScheduleTableViewCell
        let index = self.itineraries[indexPath.row]
        cell.updateName(item: index)
        return cell
    }
}
