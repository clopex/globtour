//
//  StationMapViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/17/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import Polyline

enum Location {
    case startLocation
    case destinationLocation
}

class StationMapViewController: UIViewController {

    
    var locationManager = CLLocationManager()
    var locationSelected = Location.startLocation
    
    var waypoints = [Waypoint]()
    let webAPIController = WebApiController()
    var prodajaID = ""
    
    var locationStart = CLLocation()
    var locationEnd = CLLocation()
    var isForRoute = false
    var stationForMark: Station!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webAPIController.delegate = self
        self.view.addSubview(googleMaps)
        _ = googleMaps.anchor(self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        if isForRoute {
            setupProgresView()
            activityIndicator.startAnimating()
            webAPIController.getMapStations(prodajaID: prodajaID)
        } else {
            let latitude = stationForMark.stationGpsX!
            let longitude = stationForMark.stationGpsY!
            let title = stationForMark.name!
            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10)
            googleMaps.camera = camera
            createMarker(titleMarker: title, latitude: latitude, longitude: longitude)
        }
    }
    
    func setupProgresView() {
        activityIndicator.color = PRIMARY_COLOR
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
    
    var markerArray = [GMSMarker]()
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let marker = GMSMarker()
        markerArray.append(marker)
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.map = googleMaps
    }
    
    lazy var googleMaps: GMSMapView = {
        let map = GMSMapView()
        map.accessibilityElementsHidden = false
        map.isMyLocationEnabled = true
        map.settings.compassButton = true
        map.settings.zoomGestures = true
        map.settings.myLocationButton = true
        return map
    }()
}

extension StationMapViewController: GMSMapViewDelegate {
    
    // MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        googleMaps.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        googleMaps.isMyLocationEnabled = true
        
        if (gesture) {
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        googleMaps.isMyLocationEnabled = true
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("COORDINATE \(coordinate)") // when you tapped coordinate
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        googleMaps.isMyLocationEnabled = true
        googleMaps.selectedMarker = nil
        return false
    }
}

extension StationMapViewController: WebApiControllerDelegate {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?) {
        
        if let data = result as? [Waypoint] {
            waypoints = data
            var wayPointsString = "optimize:true"
            var index = 0
            for item in data {
                index = index + 1
                let title = item.stanNAZ!
                let latitude = item.latitude!
                
                let longitude = item.longitude!
                if index < 23 {
                    wayPointsString = "\(wayPointsString)|\(latitude),\(longitude)"
                }
                self.createMarker(titleMarker: title, latitude: latitude, longitude: longitude)
            }
            guard let latitudeOrigin = data.first?.latitude else { return }
            guard let longitudeOrigin = data.first?.longitude else { return }
            guard let latitudeDestinaion = data.last?.latitude else { return }
            guard let longitudeDestination = data.last?.longitude else { return }
            
            self.webAPIController.drawRouteOnMap(waypoints: wayPointsString, originLatitude: latitudeOrigin, originLongitude: longitudeOrigin, destinationLatitude: latitudeDestinaion, destinationLongitude: longitudeDestination)
            
        }
        
        if let routes = result as? [JSON] {
            for route in routes {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyline = GMSPolyline.init(path: path)
                polyline.strokeWidth = 7
                polyline.strokeColor = PRIMARY_COLOR
                polyline.map = self.googleMaps
            }
            var bounds = GMSCoordinateBounds()
            for marker in markerArray {
                bounds = bounds.includingCoordinate(marker.position)
            }
            
            self.googleMaps.animate(with: GMSCameraUpdate.fit(bounds))
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?) {
        self.activityIndicator.stopAnimating()
        print(error?.localizedDescription ?? "Error")
    }
}
