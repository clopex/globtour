//
//  MapKitViewController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/20/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import MapKit

class MapKitViewController: UIViewController {
    
    var annotations: Array = [Waypoints]()
    var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(mapView)
        _ = mapView.anchor(self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        mapView.delegate = self
        zoomToRegion()
        getAnnotations()
        mapView.addAnnotations(annotations)
        addPoints()
    }
    
    func addPoints() {
        for item in annotations {
            points.append(item.coordinate)
        }
        
        let polyline = MKPolyline(coordinates: &points, count: points.count)
        mapView.add(polyline)
    }
    
    func getAnnotations() {
        let first = Waypoints(latitude: 45.803602, longitude: 15.993484)
        first.title = "Zagreb"
        let second = Waypoints(latitude: 45.143879, longitude: 17.254325)
        second.title = "Gradiska"
        let third = Waypoints(latitude: 45.052605, longitude: 17.307665)
        third.title = "Nova Topola"
        let fourth = Waypoints(latitude: 44.90811, longitude: 17.301135)
        fourth.title = "Laktaši"
        self.annotations.append(first)
        self.annotations.append(second)
        self.annotations.append(third)
        self.annotations.append(fourth)
    }
    
    func zoomToRegion() {
        
        let location = CLLocationCoordinate2D(latitude: 45.803602, longitude: 15.993484)
        
        let region = MKCoordinateRegionMakeWithDistance(location, 10000.0, 10000.0)
        
        mapView.setRegion(region, animated: true)
    }

    lazy var mapView: MKMapView = {
        let map = MKMapView()
        return map
    }()
}

extension MapKitViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
        if overlay is MKPolyline {
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            
        }
        return polylineRenderer
    }
}
