//
//  UIHelper.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class UIHelper {
    
    static func embeddedInNavigationController(viewController: UIViewController) -> UINavigationController {
        let navigationColtroller = UINavigationController(rootViewController: viewController)
        let navigationBar = navigationColtroller.navigationBar
        //        navigationBar.barTintColor = UIHelper.navigationBarBackgroundColor
        navigationBar.isTranslucent = false
        navigationBar.tintColor = UIColor.white
        navigationBar.barStyle = .black
        navigationBar.shadowImage = UIImage()
        return navigationColtroller
    }
    
    static var defaultAppFont: UIFont {
        return UIFont(name: "SairaCondensed-Medium", size: 18)!
    }
    
    static var defaultAppFontSize14: UIFont {
        return UIFont(name: "SairaCondensed-Medium", size: 14)!
    }
    
    static var defaultAppFontSize24: UIFont {
        return UIFont(name: "SairaCondensed-Medium", size: 24)!
    }
    
    static func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    static func checkEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let checkEmail = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return checkEmail.evaluate(with: email)
    }
    
    static func getHourAndTime(stringDate: String) -> String{
        let stringDate = stringDate
        let date = stringDate.toDate(dateFormat: "yyyy.MM.dd'T'HH:mm:ss")
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        var hourString = ""
        var minuteString = ""
        if hour < 10 {
            hourString = "0\(hour)"
        } else {
            hourString = "\(hour)"
        }
        if minute < 10 {
            minuteString = "0\(minute)"
        } else {
            minuteString = "\(minute)"
        }
        return "\(hourString):\(minuteString)"
    }
    
    static func getDayAndMonth(stringDate: String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone.current //TimeZone(abbreviation: "GMT+0:00")
        dateFormat.dateFormat = DATE_FORMAT_TOP
        guard let shortDate = dateFormat.date(from: stringDate) else {return ""}
        dateFormat.dateFormat = "dd.MM"
        return dateFormat.string(from: shortDate)
    }
    
    static func substring(string: String, fromIndex: Int, toIndex: Int) -> Double? {
        if fromIndex < toIndex && toIndex < string.count {
            let startIndex = string.index(string.startIndex, offsetBy: fromIndex)
            let endIndex = string.index(string.startIndex, offsetBy: toIndex)
            return Double(String(string[startIndex..<endIndex]))
        }else{
            return nil
        }
    }
    
    static func compareDates(dateOne: String, dateTwo: String) -> Bool {
        var compareStatus = false
        let dateOneConverted = dateOne.toDate(dateFormat: DATE_FORMAT)
        let dateTwoConverted = dateTwo.toDate(dateFormat: DATE_FORMAT)
        let order = Calendar.current.compare(dateOneConverted, to: dateTwoConverted, toGranularity: .day)
        switch order {
        case .orderedAscending:
            compareStatus = false
        case .orderedDescending:
            compareStatus = true
        case .orderedSame:
            compareStatus = false
        }
        return compareStatus
    }
}
