//
//  Constants.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import Alamofire

var allStations = [Station]()
var _allDepartures = [Departure]()

let PRIMARY_COLOR = UIColor.rgb(red: 236, green: 113, blue: 60)
let TOP_TAB_COLOR = UIColor.rgb(red: 45, green: 48, blue: 49)
let REZERVATION_COLOR = UIColor.rgb(red: 119, green: 199, blue: 76)
let WEB_USERNAME = "android_to_api"
let WEB_PASSWORD = "sadwE#4cd!dscs4CG"
let GOOGLE_API = "AIzaSyD3E7NjQY_zDzw_7P0T4nOtBvZPRjwAZ_U"
let ERROR_MSG = "The network connection was lost."

let DATE_FORMAT = "yyyy-MM-dd"
let DATE_FORMAT_VISUAL = "dd.MM.yyyy"
let DATE_FORMAT_TOP = "yyyy.MM.dd'T'HH:mm:ss"

let SHOP_ID = "CROBUSAUTO"
let SECRET_KEY = "42eaeaf006ff4B"
let SHOP_ID_TOKEN = "CROBUSTOK"
let SECRET_KEY_TOKEN = "09466adcc3cd4M"

let CUSTOMER_ADDRESS = "Savska bb"
let CUSTOMER_CITY = "Zagreb"
let CUSTOMER_COUNTRY = "Hrvatska"
let CUSTOMER_ZIP = "10000"

let RETURN_ERROR_URL = "http://globtour.knok.ba/PaymentError"
let RETURN_URL = "http://globtour.knok.ba/PaymentSuccess"

let DEFAULT_PASSWORD = "unknownuser"

let SQL_ERROR = "sql_err_0058"

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
}
