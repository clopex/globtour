//
//  BasketTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/24/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class BasketTableViewCell: UITableViewCell {

    @IBOutlet weak var ticketTypeSum: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateData(item: ListTicket) {
        let currencyName = UserDefaults.standard.getCurrencyName()
        ticketTypeSum.text = item.title
        price.text = "\(item.price) \(currencyName)"
    }

}
