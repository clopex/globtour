//
//  SwitchTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class SwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var termsLbl: UILabel!
    @IBOutlet weak var onOffSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static func createNewInstance() -> SwitchTableViewCell {
        return Bundle.main.loadNibNamed("SwitchTableViewCell", owner: nil, options: nil)![0] as! SwitchTableViewCell
    }
    
    func isTermsAccepted() -> Bool {
        if onOffSwitch.isOn {
            return true
        } else {
            return false
        }
    }
}
