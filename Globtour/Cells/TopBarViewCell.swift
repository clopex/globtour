//
//  TopBarViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/19/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class TopBarViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bottomIndicator: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                bottomIndicator.isHidden = false
            } else {
                bottomIndicator.isHidden = true
            }
        }
    }
    
    func updateMain(item: DayList) {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: item.date)
        let day = calendar.component(.day, from: item.date)
    
        if item.isSelected {
            bottomIndicator.isHidden = false
        } else {
            bottomIndicator.isHidden = true
        }
        dateLabel.text = "\(day).\(month)."
    }

}
