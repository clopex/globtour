//
//  TicketTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/7/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import UIKit

class TicketTableViewCell: UITableViewCell {
    
    let _dateFormat = "yyyy.MM.dd'T'HH:mm:ss"
    
    @IBOutlet weak var dotsImage: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var showBtn: UIButton!
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var timeFromLbl: UILabel!
    @IBOutlet weak var timeToLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func updateUI(item: MyTicketsResponse) {
        let isReserved = item.rezerviran ?? false
        if !isReserved {
            dotsImage.isHidden = true
            showBtn.setTitle("reserve".localized().uppercased(), for: .normal)
            showBtn.backgroundColor = REZERVATION_COLOR
            let departureStation = item.stanNazp ?? ""
            let arrivalStation = item.stanNazd ?? ""
            destinationLbl.text = "\(departureStation) - \(arrivalStation)"
            let departureTime = item.polazDtm ?? ""
            let arrivaltTime = item.dolazDtm ?? ""
            let priceAmount = item.iznos ?? 0.0
            let price = item.vltaOzn ?? ""
            let departureTimeMs = UIHelper.substring(string: departureTime, fromIndex: 6, toIndex: 19)
            let arrivalTimeMs = UIHelper.substring(string: arrivaltTime, fromIndex: 6, toIndex: 19)
            
            let depratureDate = Date.init(timeIntervalSince1970: (departureTimeMs! / 1000.0))
            let arrivalDate = Date.init(timeIntervalSince1970: (arrivalTimeMs! / 1000.0))
            timeFromLbl.text = "return_departure".localized().uppercased()
            timeToLbl.isHidden = true
            arrowImg.isHidden = true
            priceLbl.text = "\(priceAmount) \(price)"
            let interval = arrivalDate.timeIntervalSince(depratureDate)
            durationLbl.text = interval.stringTime
        } else {
            showBtn.setTitle("show_ticket".localized().uppercased(), for: .normal)
            showBtn.backgroundColor = PRIMARY_COLOR
            let departureStation = item.stanNazp ?? ""
            let arrivalStation = item.stanNazd ?? ""
            destinationLbl.text = "\(departureStation) - \(arrivalStation)"
            let departureTime = item.polazDtm ?? ""
            let arrivaltTime = item.dolazDtm ?? ""
            let priceAmount = item.iznos ?? 0.0
            let price = item.vltaOzn ?? ""
            let departureTimeMs = UIHelper.substring(string: departureTime, fromIndex: 6, toIndex: 19)
            let arrivalTimeMs = UIHelper.substring(string: arrivaltTime, fromIndex: 6, toIndex: 19)
            
            let depratureDate = Date.init(timeIntervalSince1970: (departureTimeMs! / 1000.0))
            let arrivalDate = Date.init(timeIntervalSince1970: (arrivalTimeMs! / 1000.0))
            let dateStart = UIHelper.getDayAndMonth(stringDate: depratureDate.toString(dateFormat: _dateFormat))
            let dateEnd = UIHelper.getDayAndMonth(stringDate: arrivalDate.toString(dateFormat: _dateFormat))
            let timeStart = UIHelper.getHourAndTime(stringDate: depratureDate.toString(dateFormat: _dateFormat))
            let timeEnd = UIHelper.getHourAndTime(stringDate: arrivalDate.toString(dateFormat: _dateFormat))
            timeFromLbl.text = "\(dateStart). | \(timeStart)"
            timeToLbl.text = "\(dateEnd). | \(timeEnd)"
            priceLbl.text = "\(priceAmount) \(price)"
            let interval = arrivalDate.timeIntervalSince(depratureDate)
            durationLbl.text = interval.stringTime
        }
    }
}
