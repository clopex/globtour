//
//  HeaderCollectionView.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/16/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class HeaderCollectionView: UICollectionReusableView {
    
    let title: UILabel = {
        let name = UILabel()
        name.text = "Route_info".localized()
        name.font = UIHelper.defaultAppFont
        name.textColor = UIColor.black
        return name
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        addSubview(title)
        _ = title.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        title.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
}

class HeaderCollectionViewEmpty: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
}

