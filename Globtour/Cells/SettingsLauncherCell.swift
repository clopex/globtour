//
//  SettingsLauncherCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/15/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class SettingsLauncherCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func createNewInstance() -> SettingsLauncherCell {
        return Bundle.main.loadNibNamed("SettingsLauncherCell", owner: nil, options: nil)![0] as! SettingsLauncherCell
    }

}
