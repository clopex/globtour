//
//  PhoneTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/16/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class PhoneTableViewCell: UITableViewCell {

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var wrongSign: UIImageView!
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var flagImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inputTextField.delegate = self
    }

    static func createNewInstance() -> PhoneTableViewCell {
        return Bundle.main.loadNibNamed("PhoneTableViewCell", owner: nil, options: nil)![0] as! PhoneTableViewCell
    }

    func getText() -> String? {
        if let text = inputTextField.text,
            !text.isEmptyOrWhitespace {
            return text
        }
        return nil
    }
    
    func showWrong() {
        wrongSign.isHidden = false
    }
    
    func hideWrong() {
        wrongSign.isHidden = true
    }
    
    func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
}

extension PhoneTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        wrongSign.isHidden = true
    }
}
