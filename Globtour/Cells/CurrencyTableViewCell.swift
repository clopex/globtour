//
//  CurrencyTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/17/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateData(item: CurrencyData) {
        self.nameLabel.text = "\(item.shortName!) - \(item.name!)"
    }
    
    func updateTranslations(item: Language) {
        guard let name = item.languageName else {return}
        self.nameLabel.text = "\(name)"
    }
    
}
