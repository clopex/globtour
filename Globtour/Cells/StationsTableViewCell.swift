//
//  StationsTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/8/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class StationsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stationLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateName(item: Station) {
        stationLbl.text = item.name
    }
}
