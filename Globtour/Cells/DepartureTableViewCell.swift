//
//  DepartureTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class DepartureTableViewCell: UITableViewCell {
    
    let _dateFormat = "yyyy.MM.dd'T'HH:mm:ss"

    @IBOutlet weak var timeStart: UILabel!
    @IBOutlet weak var timeEnd: UILabel!
    @IBOutlet weak var nameBus: UILabel!
    @IBOutlet weak var rideTime: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var dotsImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateName(item: Departure) {
        let currencyName = UserDefaults.standard.getCurrencyName()
        nameBus.text = item.transportCompanyShortName
        timeStart.text = UIHelper.getHourAndTime(stringDate: item.departureTime!)
        timeEnd.text = UIHelper.getHourAndTime(stringDate: item.arrivalTime!)
        rideTime.text = item.duration
        price.text = "\(item.baseOnWayPrice[0].amount ?? 0.0) \(currencyName.uppercased())"
    }
}
