//
//  SettingsTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/8/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var settingsImage: UIImageView!
    @IBOutlet weak var settingsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func createNewInstance() -> SettingsTableViewCell {
        return Bundle.main.loadNibNamed("SettingsTableViewCell", owner: nil, options: nil)![0] as! SettingsTableViewCell
    }
    
}
