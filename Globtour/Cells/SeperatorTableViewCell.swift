//
//  SeperatorTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/8/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class SeperatorTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func createNewInstance() -> SeperatorTableViewCell {
        return Bundle.main.loadNibNamed("SeperatorTableViewCell", owner: nil, options: nil)![0] as! SeperatorTableViewCell
    }
}
