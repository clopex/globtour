//
//  PassengerTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/22/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class PassengerTableViewCell: UITableViewCell {

    @IBOutlet weak var passengerName: UILabel!
    @IBOutlet weak var ticketPrice: UILabel!
    @IBOutlet weak var tipOfTicket: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateData(item: Passanger) {
        let currencyName = UserDefaults.standard.getCurrencyName()
        passengerName.text = "\(item.name!) \(item.surname!)"
        tipOfTicket.text = item.ticketType
        ticketPrice.text = "\(item.price!) \(currencyName)"
    }

}
