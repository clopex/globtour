//
//  RegistrationTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class RegistrationTableViewCell: UITableViewCell {

    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var wrongSign: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inputTextField.delegate = self
    }

    static func createNewInstance() -> RegistrationTableViewCell {
        return Bundle.main.loadNibNamed("RegistrationTableViewCell", owner: nil, options: nil)![0] as! RegistrationTableViewCell
    }
    
    func getText() -> String? {
        if let text = inputTextField.text,
            !text.isEmptyOrWhitespace {
            return text
        }
        return nil
    }
    
    func isEmailValid() -> Bool {
        let email = inputTextField.text
        if ((email?.isEmpty)! || !checkEmail(email: email!)) {
            wrongSign.isHidden = false
            return false
        } else {
            wrongSign.isHidden = true
            return true
        }
    }
    
    func showWrong() {
        wrongSign.isHidden = false
    }
    
    func hideWrong() {
        wrongSign.isHidden = true
    }
    
    func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func checkEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let checkEmail = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return checkEmail.evaluate(with: email)
    }
}

extension RegistrationTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        wrongSign.isHidden = true
    }
}
