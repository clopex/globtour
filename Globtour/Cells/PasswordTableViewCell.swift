//
//  PasswordTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/27/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class PasswordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var cellLbl: UILabel!
    @IBOutlet weak var cellTextField: UITextField!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var wrongSign: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        cellTextField.delegate = self
    }

    static func createNewInstance() -> PasswordTableViewCell {
        return Bundle.main.loadNibNamed("PasswordTableViewCell", owner: nil, options: nil)![0] as! PasswordTableViewCell
    }
    
    func getText() -> String? {
        if let text = cellTextField.text,
            !text.isEmptyOrWhitespace {
            return text
        }
        return nil
    }
    
    func showWrong() {
        wrongSign.isHidden = false
    }
    
    func hideWrong() {
        wrongSign.isHidden = true
    }
}

extension PasswordTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        wrongSign.isHidden = true
    }
}
