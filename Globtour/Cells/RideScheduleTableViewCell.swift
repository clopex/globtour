//
//  RideScheduleTableViewCell.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/16/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit

class RideScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var stationLbl: UILabel!
    @IBOutlet weak var arrivalLbl: UILabel!
    @IBOutlet weak var departureLbl: UILabel!
    
    let _dateFormat = "yyyy.MM.dd'T'HH:mm:ss"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateName(item: Itinerary) {
        stationLbl.text = getStationName(stationId: item.stationId!)
        arrivalLbl.text = UIHelper.getHourAndTime(stringDate: item.arrival!)
        departureLbl.text = UIHelper.getHourAndTime(stringDate: item.departure!)
    }
    
    func getStationName(stationId: Int) -> String {
        for item in allStations {
            if stationId == item.id {
                return item.name!
            }
        }
        return ""
    }
}
