//
//  WebApiController.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/7/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol WebApiControllerDelegate: class {
    func webAPIControllerDidFinish(webApiController: WebApiController, result: Any?)
    func webAPIControllerFailedWithError(webApiController: WebApiController, error: Error?, userInfo: Any?)
}

class WebApiController {
    
    static let urlBase = "https://api.bookbybus.com/test/BookingService.svc/v1/"
    static let urlPayment = "https://test.wspay.biz/WsPayAuto.aspx"
    static let urlSendEmail = "http://globtour.knok.ba/PaymentSuccess/SendEmail"
    static let urlMinimunVersion = "http://globtour.knok.ba/MobileDevice/MinimumRequiredIosVersion"
    
    weak var delegate: WebApiControllerDelegate?
    
    var token: String {
        return Token.shared?.token ?? ""
    }
    
    init() {
    }
    
    func getAllStations(id: Int) {
        let url = WebApiController.urlBase + "AllStationsGPS"
        let langString = UserDefaults.standard.getLanguageName()
        Alamofire.request(url, method: .get).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let stations: [Station] = self.getResults(json: swiftyJSON)
                var listOfStations = [Station]()
                for item in stations {
                    if item.lang == langString {
                        if !(item.name?.isEmpty)! {
                            if id != 0 {
                                if id != item.id {
                                    listOfStations.append(item)
                                }
                            } else {
                                listOfStations.append(item)
                            }
                        }
                    }
                }
                
                print(stations.count)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: listOfStations)
            case .failure(let error):
                print("Stations \(error)")
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: error, userInfo: nil)
            }
        }
    }
    
    func getToken() {
        let url = WebApiController.urlBase + "GetToken"
        let params = ["Username": WEB_USERNAME, "Password": WEB_PASSWORD]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let loginSession = Token(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: loginSession)
            case .failure(let err):
                print(err)
            }
        }
    }
    
    func checkUserLogin(email: String, password: String) {
        let url = WebApiController.urlBase + "GetUser/\(email)/\(password)/\(token)"
        Alamofire.request(url, method: .get).responseJSON { (dataResponse) in
            switch(dataResponse.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                if let rawData = swiftyJSON.rawString() {
                    self.delegate?.webAPIControllerDidFinish(webApiController: self, result: rawData)
                }
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
                print("User login error: \(err.localizedDescription)")
            }
        }
    }
    
    func registerUser(userData: AplicationUser) {
        let url = WebApiController.urlBase + "RegisterUserF"
        
        let dict:[String:Any] = userData.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["ApplicationUser":jsonParametar!]
        print(parametar)
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (dataResponse) in
            switch(dataResponse.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                if let rawData = swiftyJSON.rawString() {
                    self.delegate?.webAPIControllerDidFinish(webApiController: self, result: rawData)
                }
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }

    func getDepartures(stationFrom: Int, stationTo: Int, dateTrip: String, dateReturn: String, isReturn: Bool, isReturnDateKnown: Bool) {
        let url = WebApiController.urlBase + "Departures"
        let currencyId = UserDefaults.standard.getCurrencyId()
        let langString = Locale.current.languageCode
        var dict:[String:Any] = ["lang":langString!]
        dict["stationFrom"] = stationFrom
        dict["stationTo"] = stationTo
        dict["DateTrip"] = dateTrip
//        dict["DateReturn"] = nil
        if dateReturn.isEmpty {
            dict["DateReturn"] = nil
        } else {
            dict["DateReturn"] = dateReturn
        }
        dict["isReturn"] = isReturn
        dict["isReturnDateKnown"] = isReturnDateKnown
        dict["Currency"] = currencyId
        dict["Token"] = token
        
        let jsonParametar = dictToJsonString(dict: dict)
        let parameter:[String:String] = ["RequestModel":jsonParametar!]
        print("Requested Param: \(parameter)")
        
        Alamofire.request(url, method: .post, parameters: parameter).responseJSON { (responseData) in
            switch (responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let getJsonFromString = JSON.init(parseJSON: swiftyJSON.rawString()!)
//                print(getJsonFromString)
                let allDepartures = BaseDeparture(json: getJsonFromString)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: allDepartures)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getCurrencies() {
        let url = WebApiController.urlBase + "Currencies"
        Alamofire.request(url, method: .get).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let currenices: [CurrencyData] = self.getResults(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: currenices)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getAllTranslations() {
        let url = WebApiController.urlBase + "AllTranslations"
        Alamofire.request(url, method: .get).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let langauge: [Language] = self.getResults(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: langauge)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getMapStations(prodajaID: String) {
        let langString = Locale.current.languageCode
        let url = WebApiController.urlBase + "GetRoute/\(prodajaID)/\(langString!)"
        
        Alamofire.request(url, method: .get).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let waypoints: [Waypoint] = self.getResults(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: waypoints)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func changePassword(passData: Password) {
        let url = WebApiController.urlBase + "OpenChangePassword"
        let dict:[String:Any] = passData.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: data)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func changeProfileInformation(userData: AplicationUser) {
        let url = WebApiController.urlBase + "ChangeUserInfo"
        let dict:[String:Any] = userData.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: data)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getBasketInfo(data: BasketRequestModel) {
        let url = WebApiController.urlBase + "GetBasket"
        let dict:[String:Any] = data.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let getBasket: Basket = self.getResult(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: getBasket)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func paymingGatewayTransaction(data: WsPaySend) {
        let url = WebApiController.urlBase + "AddPaymingGatewayTransaction"
        let dict:[String:Any] = data.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let getWsPaySend = WsPaySend(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: getWsPaySend)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func wsPaySendTransaction(paySend: WsPaySend) {
        let url = WebApiController.urlPayment
        let params = ["CreditCardName": paySend.creditCardName!,
                      "CreditCardNumber": paySend.creditCardNumber!,
                      "CardVerificationData": paySend.cardVerificationData!,
                      "ExpirationDate": paySend.expirationDate!,
                      "IsTokenRequest": "true",
                      "PaymentPlan": paySend.paymenyPlan!,
                      "TotalAmount": paySend.totalAmount!,
                      "DateTime": paySend.dateTime!,
                      "ShopID": paySend.shopId!,
                      "ShoppingCartID": paySend.shoppingCartId!,
                      "Signature": paySend.signature!,
                      "ReturnURL": RETURN_URL,
                      "ReturnErrorURL": RETURN_ERROR_URL,
                      "CustomerFirstName": paySend.customerFirstName!,
                      "CustomerLastName": paySend.customerLastName!,
                      "CustomerAddress": paySend.customerAddress!,
                      "CustomerCity": paySend.customerCity!,
                      "CustomerZIP": paySend.customerZip!,
                      "CustomerCountry": paySend.customerCountry!,
                      "CustomerPhone": paySend.customerPhone!,
                      "CustomerEmail": paySend.customerEmail!]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers).responseString { (dataResponse) in
            switch(dataResponse.result) {
            case .success(let data):
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: data)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func updatePaymingGatewayTransaction(postPayment: PostPayment) {
        let url = WebApiController.urlBase + "UpdatePaymingGatewayTransaction"
        let dict:[String:Any] = postPayment.wsResponse!.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                if !swiftyJSON.isEmpty {
                    self.getInvoice(postPayment: postPayment)
                }
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getInvoice(postPayment: PostPayment) {
        let url = WebApiController.urlBase + "GetInvoice"
        postPayment.invoiceRequest?.approvalCode = postPayment.wsResponse?.approvalCode ?? ""
        postPayment.invoiceRequest?.cardNum = postPayment.wsResponse?.maskedPlan ?? ""
        
        let dict:[String:Any] = postPayment.invoiceRequest!.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let paymentRespone = PaymentCommitedInvoiceResponse(json: swiftyJSON)
                self.sendEmail(invoice: paymentRespone)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: true)
            case .failure(let err):
                print(err)
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func sendEmail(invoice: PaymentCommitedInvoiceResponse) {
        let userDataJSON = JSON.init(parseJSON: UserDefaults.standard.getUserData())
        let aplicationUser = AplicationUser(json: userDataJSON)
        let email = aplicationUser.email ?? ""
        let userId = aplicationUser.id ?? 0
        let faktBr = invoice.faktBroj ?? 0
        let posSif = invoice.posSif ?? 0
        let softwareVersion = invoice.softwareVersion ?? ""
        let year = invoice.year ?? 0
        let regenratePdf = invoice.regeneratePdf ?? false
        
        let url = URL(string: WebApiController.urlSendEmail)!
        
        let json = "invoiceString={\"FAKT_BRJ\":\(faktBr),\"PDF\":[],\"POS_SIF\":\(posSif),\"RegeneratePDF\":\(regenratePdf),\"SoftwareVersion\":\"\(softwareVersion)\",\"User_Id\":\(userId),\"Year\":\(year)}&emailString=\(email)"
        let body = json.data(using: .utf8)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = body
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let _ = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: response)
                print("statusCode: \(httpResponse.statusCode)")
            }
        }
        
        task.resume()
    }
    
    func getTickets(data: MyTicketsRequest) {
        let url = WebApiController.urlBase + "MyTickets"
        let dict:[String:Any] = data.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let tickets: [MyTicketsResponse] = self.getResults(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: tickets)
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getUserByName(email: String) {
        let url = WebApiController.urlBase + "GetUserByName/\(email)/\(token)"
        Alamofire.request(url, method: .get).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                if let rawData = swiftyJSON.rawString() {
                    self.delegate?.webAPIControllerDidFinish(webApiController: self, result: rawData)
                }
            case .failure(let err):
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func setTicketReservation(reservationData: ReservationRequest) {
        let url = WebApiController.urlBase + "SetReservation"
        let dict:[String:Any] = reservationData.toJSON()
        let jsonParametar = dictToJsonString(dict: dict)
        let parametar:[String:String] = ["RequestModel":jsonParametar!]
        
        Alamofire.request(url, method: .post, parameters: parametar).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                let ticketResponse: ReservationResponse = self.getResult(json: swiftyJSON)
                self.delegate?.webAPIControllerDidFinish(webApiController: self, result: ticketResponse)
            case .failure(let err):
                print(err.localizedDescription)
                self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
            }
        }
    }
    
    func getMinimumBuildVersion() {
        let url = WebApiController.urlMinimunVersion
        Alamofire.request(url, method: .get).responseJSON { (responseData) in
            switch(responseData.result) {
            case .success(let data):
                let swiftyJSON = JSON(data)
                print(swiftyJSON)
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func drawRouteOnMap(waypoints: String, originLatitude: Double, originLongitude: Double, destinationLatitude: Double, destinationLongitude: Double) {
        
        let parameters : [String : String] = ["key" : GOOGLE_API, "mode" : "driving", "origin" : "\(originLatitude),\(originLongitude)", "destination" : "\(destinationLatitude),\(destinationLongitude)", "waypoints" : waypoints]
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?")
        
        Alamofire.request(url!, method:.get, parameters: parameters)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch(response.result) {
                case .success(let data):
                    let swiftyJSON = JSON(data)
                    let routes = swiftyJSON["routes"].arrayValue
                    self.delegate?.webAPIControllerDidFinish(webApiController: self, result: routes)                    
                case .failure(let err):
                    self.delegate?.webAPIControllerFailedWithError(webApiController: self, error: err, userInfo: nil)
                }
        }
    }
    
    
// ------ Function for converting data ---------
    
    
    func jsonToData(json: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
    
    func dictToJsonString(dict: [String:Any]) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        } catch {
            return nil
        }
    }

    
    init(delegate: WebApiControllerDelegate) {
        self.delegate = delegate
    }
    
    private func getResult<T: JSONDecodable>(json: JSON) -> T {
        return T(json: json)
    }
    
    private func getResults<T: JSONDecodable>(json: JSON) -> [T] {
        return json.arrayValue.map{ T(json: $0) }
    }
    
}
