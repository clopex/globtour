//
//  Token2.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/12/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation

struct Token2: Encodable, Decodable {
    var Id: Int
    var Username: String
    var Password: String
    var Token: String
    var TokenPristup: String
    var Active: Bool
}
