//
//  ReservationRequest.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/9/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReservationRequest: NSObject {
    var userId: Int?
    var guid: String?
    var posId: Int?
    var ticketId: Int?
    var ticketYear: Int?
    var faktID: Int?
    var faktYear: Int?
    var token: String?
    
    init(userId: Int, guid: String, token: String, posId: Int, ticketId: Int, ticketYear: Int, faktId: Int, faktYear: Int) {
        self.userId = userId
        self.guid = guid
        self.token = token
        self.posId = posId
        self.ticketId = ticketId
        self.ticketYear = ticketYear
        self.faktID = faktId
        self.faktYear = faktYear
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let userId = userId {
            json["User_Id"] = userId
        }
        
        if let guid = guid {
            json["GUID"] = guid
        }
        
        if let posId = posId {
            json["Pos_ID"] = posId
        }
        
        if let ticketId = ticketId {
            json["Ticket_ID"] = ticketId
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let ticketYear = ticketYear {
            json["Ticket_Year"] = ticketYear
        }
        
        if let faktID = faktID {
            json["FAKT_ID"] = faktID
        }
        
        if let faktYear = faktYear {
            json["FAKT_Year"] = faktYear
        }
        
        return json
    }
}
