//
//  MyTicketsRequest.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/7/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class MyTicketsRequest: NSObject {
    var userId: Int?
    var agencyId: Int?
    var lang: String?
    var onlyNotReserved: Bool?
    var token: String?
    
    init(userId: Int, agencyId: Int, lang: String, onlyNotReserved: Bool, token: String) {
        self.userId = userId
        self.agencyId = agencyId
        self.lang = lang
        self.onlyNotReserved = onlyNotReserved
        self.token = token
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let userId = userId {
            json["User_Id"] = userId
        }
        
        if let agencyId = agencyId {
            json["Agency_Id"] = agencyId
        }
        
        if let lang = lang {
            json["Lang"] = lang
        }
        
        if let onlyNotReserved = onlyNotReserved {
            json["ONLY_NOT_RESERVED"] = onlyNotReserved
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        return json
    }
}
