//
//  Currency.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/17/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class CurrencyData: NSObject, JSONDecodable {
    var id: Int?
    var name: String?
    var shortName: String?
    var decimalPlaces: Int?
    
    required init(json: JSON) {
        id = json["Id"].intValue
        name = json["Name"].string
        shortName = json["ShortName"].string
        decimalPlaces = json["DecimalPlaces"].intValue
    }
    
    override init() {
        super.init()
    }
}
