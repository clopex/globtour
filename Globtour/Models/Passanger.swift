//
//  Passanger.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/23/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Passanger: NSObject {
    var name: String?
    var surname: String?
    var rowNum: Int?
    var phone: String?
    var price: Double?
    var categoryId: Int?
    var categoryName: String?
    var ticketType: String?
//    var categoryId: Int?
    
//    var name: String?
//    var ticketType: String?
//    var ticketPrice: Double?
    
    init(name: String, surname: String, rowNum: Int, phone: String, price: Double, categoryId: Int, categoryName: String, ticketType: String) {
        self.name = name
        self.surname = surname
        self.rowNum = rowNum
        self.phone = phone
        self.price = price
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.ticketType = ticketType
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let name = name {
            json["Name"] = name
        }
        
        if let surname = surname {
            json["Surname"] = surname
        }
        
        if let rowNum = rowNum {
            json["RowNum"] = rowNum
        }
        
        if let phone = phone {
            json["Phone"] = phone
        }
        
        if let price = price {
            json["price"] = price
        }
        
        if let categoryId = categoryId {
            json["Category_Id"] = categoryId
        }
        
        if let categoryName = categoryName {
            json["Category_Name"] = categoryName
        }
        
        return json
    }
    
//    init(name: String, ticketType: String, ticketPrice: Double) {
//        self.name = name
//        self.ticketType = ticketType
//        self.ticketPrice = ticketPrice
//    }
}

