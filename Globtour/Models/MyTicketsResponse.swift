//
//  MyTicketsResponse.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/7/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class MyTicketsResponse: NSObject, JSONDecodable {
    var rezerviran: Bool?
    var posSif: Int?
    var faktBrj: Int?
    var faktGod: Int?
    var faktDtm: String?
    var tiketBrj: Int?
    var tiketGod: Int?
    var putnikIme: String?
    var putnikPrezime: String?
    var putnikDok: String?
    var stanNazp: String?
    var stanNazd: String?
    var stanSifp: Int?
    var stanSifd: Int?
    var polazDtm: String?
    var dolazDtm: String?
    var vrijediDo: String?
    var iznos: Double?
    var vltaOzn: String?
    
    required init(json: JSON) {
        rezerviran = json["REZERVIRAN"].boolValue
        posSif = json["POS_SIF"].intValue
        faktBrj = json["FAKT_BRJ"].intValue
        faktGod = json["FAKT_GOD"].intValue
        faktDtm = json["FAKT_DTM"].string
        tiketBrj = json["TIKET_BRJ"].intValue
        tiketGod = json["TIKET_GOD"].intValue
        putnikIme = json["PUTNIK_IME"].string
        putnikPrezime = json["PUTNIK_PRE"].string
        putnikDok = json["PUTNIK_DOK"].string
        stanNazp = json["STAN_NAZP"].string
        stanNazd = json["STAN_NAZD"].string
        stanSifp = json["STAN_SIFP"].intValue
        stanSifd = json["STAN_SIFD"].intValue
        polazDtm = json["POLAZ_DTM"].string
        dolazDtm = json["DOLAZ_DTM"].string
        vrijediDo = json["VRIJEDI_DO"].string
        iznos = json["IZNOS"].doubleValue
        vltaOzn = json["VLTA_OZN"].string
    }
    
    override init() {
        super.init()
    }
}
