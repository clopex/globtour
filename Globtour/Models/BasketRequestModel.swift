//
//  BasketRequestModel.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/31/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class BasketRequestModel: NSObject {
    
    var id: Int?
    var guid: String?
    var guidReturn: String?
    var isRoundTrip: Bool?
    var token: String?
    var currencyId: Int?
    var passengers: [Passanger]?
    
    init(id: Int, guid: String, guidReturn: String, isRoundTrip: Bool, token: String, currencyId: Int, passengers: [Passanger]) {
        self.id = id
        self.guid = guid
        self.guidReturn = guidReturn
        self.isRoundTrip = isRoundTrip
        self.token = token
        self.currencyId = currencyId
        self.passengers = passengers
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let id = id {
            json["User_Id"] = id
        }
        
        if let guid = guid {
            json["GUID"] = guid
        }
        
        if let guidReturn = guidReturn {
            json["GUID_POVR"] = guidReturn
        }
        
        if let isRoundTrip = isRoundTrip {
            json["IsRoundTrip"] = isRoundTrip
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let currencyId = currencyId {
            json["Currency_ID"] = currencyId
        }
        
        if let passangers = passengers {
            var passengersDict: [Dictionary<String, Any>] = []
            for item in passangers {
                passengersDict.append(item.toJSON())
            }
            json["Passengers"] = passengersDict
        }
        
        return json
    }
    
}

