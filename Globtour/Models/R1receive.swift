//
//  R1receive.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/2/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation

class R1recive: NSObject {
    var nameSurname: String?
    var companyName: String?
    var companyAddress: String?
    var companyOib: String?
    
    init(nameSurname: String, companyName: String, companyAddress: String, companyOib: String) {
        self.nameSurname = nameSurname
        self.companyAddress = companyAddress
        self.companyName = companyName
        self.companyOib = companyOib
    }
    
}
