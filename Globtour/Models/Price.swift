//
//  Price.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/13/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Price: NSObject, JSONDecodable {
    var currencyId: Int?
    var currencyName: Int?
    var amount: Double?
    var categoryId: Int?
    var categoryName: Int?
    var basePrice: Double?
    var exchangeRate: Double?
    
    required init(json: JSON) {
        currencyId = json["CurrencyId"].intValue
        currencyName = json["CurrencyName"].intValue
        amount = json["Amount"].doubleValue
        categoryId = json["CategoryId"].intValue
        categoryName = json["CategoryName"].intValue
        basePrice = json["BasePrice"].doubleValue
        exchangeRate = json["ExchangeRate"].doubleValue
    }
    
    override init() {
        super.init()
    }
}
