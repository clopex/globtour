//
//  PriceDetail.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/13/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class PriceDetail: NSObject, JSONDecodable {
    var kategNaz: String?
    var vrkaNaz: String?
    var lnjaKategPop: Int?
    var cijena: Double?
    var language: String?
    var vltaSif: Int?
    var tecVri: Double?
    var vrkaSif: Int?
    var kategSif: Int?
    var lnjaKategDne: String?
    var iznos01: Double?
    var iznos04: Double?
    
    required init(json: JSON) {
        kategNaz = json["KATEG_NAZ"].string
        vrkaNaz = json["VRKA_NAZ"].string
        lnjaKategPop = json["LNJA_KATEG_POP"].intValue
        cijena = json["cijena"].doubleValue
        language = json["Language"].string
        vltaSif = json["VLTA_SIF"].intValue
        tecVri = json["TEC_VRI"].doubleValue
        vrkaSif = json["VRKA_SIF"].intValue
        kategSif = json["KATEG_SIF"].intValue
        lnjaKategDne = json["LNJA_KATEG_DNE"].string
        iznos01 = json["IZNOS_01"].doubleValue
        iznos04 = json["IZNOS_04"].doubleValue
    }
    
    init(category: String, price: Double, language: String, kategSif: Int) {
        self.kategNaz = category
        self.cijena = price
        self.language = language
        self.kategSif = kategSif
    }
    
    override init() {
        super.init()
    }
}
