//
//  Profile.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/12/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Profile: NSObject, JSONDecodable {
    var id: Int?
    var acceptTerms: Bool?
    var email: String?
    var emailConfirmed: Bool?
    var accessFailedCount: Int?
    var lockoutEnabled: Bool?
    var lockoutEndDate: String?
    var passwordHash: String?
    var phoneNumber: String?
    var phoneNumberConfirmed: Bool?
    var username: String?
    var name: String?
    var surname: String?
    var marketing: Bool?
    var token: String?
    var securityStamp: String?
    var agency_Id: Int?
    var pos_Id: Int?
    var aGEN_ADR: String?
    var aGEN_NAZ: String?
    var aGEN_OIB: String?
    var tableName: String?
    
    required init(json: JSON) {
        id = json["UserId"].intValue
        acceptTerms = json["AcceptThermsOfUse"].bool
        email = json["Email"].string
        emailConfirmed = json["EmailConfirmed"].bool
        accessFailedCount = json["AccessFailedCount"].intValue
        lockoutEnabled = json["LockoutEnabled"].bool
        lockoutEndDate = json["LockoutEndDateUtc"].string
        passwordHash = json["PasswordHash"].string
        phoneNumber = json["PhoneNumber"].string
        phoneNumberConfirmed = json["PhoneNumberConfirmed"].bool
        username = json["Username"].string
        name = json["Name"].string
        surname = json["Surname"].string
        marketing = json["Marketing"].bool
        token = json["Token"].string
        securityStamp = json["SecurityStamp"].string
        agency_Id = json["Agency_Id"].intValue
        pos_Id = json["Pos_Id"].intValue
        aGEN_ADR = json["AGEN_ADR"].string
        aGEN_NAZ = json["AGEN_NAZ"].string
        aGEN_OIB = json["AGEN_OIB"].string
        tableName = json["tableName"].string
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let marketing = marketing {
            json["Marketing"] = marketing
        }
        
        if let username = username {
            json["Username"] = username
        }
        
        if let phoneNumber = phoneNumber {
            json["PhoneNumber"] = phoneNumber
        }
        
        if let securityStamp = securityStamp {
            json["SecurityStamp"] = securityStamp
        }
        
        if let aGEN_ADR = aGEN_ADR {
            json["AGEN_ADR"] = aGEN_ADR
        }
        
        if let aGEN_NAZ = aGEN_NAZ {
            json["AGEN_NAZ"] = aGEN_NAZ
        }
        
        if let aGEN_OIB = aGEN_OIB {
            json["AGEN_OIB"] = aGEN_OIB
        }
        
        if let tableName = tableName {
            json["tableName"] = tableName
        }
        
        if let id = id {
            json["UserId"] = id
        }
        
        if let acceptTerms = acceptTerms {
            json["AcceptThermsOfUse"] = acceptTerms
        }
        
        if let email = email {
            json["Email"] = email
        }
        
        if let emailConfirmed = emailConfirmed {
            json["EmailConfirmed"] = emailConfirmed
        }
        
        if let accessFailedCount = accessFailedCount {
            json["AccessFailedCount"] = accessFailedCount
        }
        
        if let lockoutEndDate = lockoutEndDate {
            json["LockoutEndDateUtc"] = lockoutEndDate
        }
        
        if let name = name {
            json["Name"] = name
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let surname = surname {
            json["Surname"] = surname
        }
        
        if let agency_Id = agency_Id {
            json["Agency_Id"] = agency_Id
        }
        
        if let passwordHash = passwordHash {
            json["PasswordHash"] = passwordHash
        }
        
        if let phoneConf = phoneNumberConfirmed {
            json["PhoneNumberConfirmed"] = phoneConf
        }
        
        
        if let lockOutEnable = lockoutEnabled {
            json["LockoutEnabled"] = lockOutEnable
        }
        
        if let pos_Id = pos_Id {
            json["Pos_Id"] = pos_Id
        }
        
        return json
    }
    
    override init() {
        super.init()
    }
}



