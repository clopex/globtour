//
//  Waypoints.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/20/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import UIKit
import SwiftyJSON

class Waypoint: NSObject, JSONDecodable {
    var polazOpisRbr: Int?
    var lnjaStanRouteRbe: Int?
    var stanNAZ: String?
    var stanAdrADR: String?
    var stanGrad: String?
    var latitude: Double?
    var longitude: Double?
    
    required init(json: JSON) {
        polazOpisRbr = json["POLAZ_OPIS_RBR"].intValue
        lnjaStanRouteRbe = json["LNJA_STAN_ROUTE_RBR"].intValue
        stanNAZ = json["STAN_NAZ"].string
        stanAdrADR = json["STAN_ADR_ADR"].string
        stanGrad = json["STAN_ADR_GRAD"].string
        latitude = json["STAN_ADR_GPSX"].doubleValue
        longitude = json["STAN_ADR_GPSY"].doubleValue
    }
    
    override init() {
        super.init()
    }
}
