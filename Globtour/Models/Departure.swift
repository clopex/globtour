//
//  Departure.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/13/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Departure: NSObject, JSONDecodable {
    var prodajeFindGUID: String?
    var polazSif: Int?
    var polazSasParal: Int?
    var polazSasSeg: Int?
    var polazOpisRbrPol: Int?
    var polazOpisRbrDol: Int?
    var danVoznje: String?
    var departureTime: String?
    var varFind: Int?
    var arrivalTime: String?
    var itineraryInfo: String?
    var transportCompanyShortName: String?
    var transportCompanyName: String?
    var discountInfo: String?
    var transferInfo: String?
    var positionsLeft: Int?
    var popust01: Double?
    var popust04: Double?
    var duration: String?

    var busEquipment: [String] = []
//    var departuresArray: [Departure]?
    var prices: [PriceDetail] = []
    var transfers: [Transfer] = []
    var baseOnWayPrice: [Price] = []
    var itineraries: [Itinerary] = []

    required init(json: JSON) {

        prodajeFindGUID = json["PRODAJA_FIND_GUID"].string
        polazSif = json["POLAZ_SIF"].intValue
        polazSasParal = json["POLAZ_SAS_PARAL"].intValue
        polazSasSeg = json["POLAZ_SAS_SEGM"].intValue
        polazOpisRbrPol = json["POLAZ_OPIS_RBR_pol"].intValue
        polazOpisRbrDol = json["POLAZ_OPIS_RBR_dol"].intValue
        danVoznje = json["DAN_VOZNJE"].string
        departureTime = json["DepartureTime"].string
        varFind = json["VER_FIND"].intValue
        arrivalTime = json["ArrivalTime"].string
        itineraryInfo = json["ItineraryInfo"].string
        transportCompanyShortName = json["TransportCompanyShortName"].string
        transportCompanyName = json["TransportCompanyName"].string
        discountInfo = json["DiscountInfo"].string
        transferInfo = json["TransferInfo"].string
        positionsLeft = json["PositionsLeft"].intValue
        popust01 = json["POPUST_01"].doubleValue
        popust04 = json["POPUST_04"].doubleValue
        duration = json["Duration"].string

        for item in json["BusEquipment"].arrayValue {
            busEquipment.append(item.stringValue)
        }

        for item in json["BaseOnWayPrice"].arrayValue {
            let nameFromJson = Price(json: item)
            baseOnWayPrice.append(nameFromJson)
        }

        for item in json["Itineraries"].arrayValue {
            let nameFromJson = Itinerary(json: item)
            itineraries.append(nameFromJson)
        }

        for item in json["Transfers"].arrayValue {
            let nameFromJson = Transfer(json: item)
            transfers.append(nameFromJson)
        }

        for item in json["Prices"].arrayValue {
            let nameFromJson = PriceDetail(json: item)
            prices.append(nameFromJson)
        }

    }

    override init() {
        super.init()
    }
}

