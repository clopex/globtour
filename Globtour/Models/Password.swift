//
//  Password.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/30/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Password: NSObject, JSONDecodable {
    var id: Int?
    var oldPassword: String?
    var token: String?
    var newPassword: String?
    
    required init(json: JSON) {
        id = json["Id"].intValue
        oldPassword = json["PasswordOld"].string
        token = json["Token"].string
        newPassword = json["PasswordNew"].string
    }
    
    init(_id: Int, _oldPassword: String, _newPassword: String, _token: String) {
        self.oldPassword = _oldPassword
        self.newPassword = _newPassword
        self.id = _id
        self.token = _token
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let id = id {
            json["Id"] = id
        }
        
        if let oldPassword = oldPassword {
            json["PasswordOld"] = oldPassword
        }
        
        if let newPassword = newPassword {
            json["PasswordNew"] = newPassword
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        return json
    }
    
    override init() {
        super.init()
    }
}
