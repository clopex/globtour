//
//  Transfer.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/13/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Transfer: NSObject, JSONDecodable {
    var station: Station?
    var departureTime: String?
    var arrivalTime: String?
    
    required init(json: JSON) {
//        station = json["KATEG_NAZ"].string
        departureTime = json["DepartureTime"].string
        arrivalTime = json["ArrivalTime"].string
    }
    
    override init() {
        super.init()
    }
}
