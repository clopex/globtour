//
//  StationDelegate.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/9/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation

@objc protocol StationDelegate: class {
    
    func pickerDidFinishWithResult(sender: StationsViewController, pickerItems: Station)
    @objc optional func pikcerDidSelectValue(sender: StationsViewController, pickerItems: Station)
    @objc optional func pickerDidCancel(sender: StationsViewController)
}

