//
//  Itinerary.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/13/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Itinerary: NSObject, JSONDecodable {
    var stationId: Int?
    var gPSX: Double?
    var gPSY: Double?
    var polazOpisRBR: Int?
    var departure: String?
    var arrival: String?
    var transfer: Int?
    var equipment: [String]?
    var companyId: Int?
    var companyName: String?
    var name: [Name]?
    var polazOpisPeron: String?
    
    required init(json: JSON) {
        stationId = json["StationId"].intValue
        gPSX = json["GPSX"].doubleValue
        gPSY = json["GPSY"].doubleValue
        polazOpisRBR = json["POLAZ_OPIS_RBR"].intValue
        departure = json["Departure"].string
        arrival = json["Arrival"].string
        transfer = json["Transfer"].intValue
        equipment = json["Equipment"].arrayValue.map { $0.stringValue}
        companyId = json["CompanyId"].intValue
        companyName = json["CompanyName"].string
        polazOpisPeron = json["Polaz_Opis_Peron"].string
        for item in json["Name"].arrayValue {
            let nameFromJson = Name(json: item)
            name?.append(nameFromJson)
        }
    }
    
    override init() {
        super.init()
    }
}


