//
//  AplicationUser.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/16/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class AplicationUser: NSObject, JSONDecodable {
    var googleId: String?
    var email: String?
    var emailConfirmed: Bool?
    var accessFailedCount: Int?
    var id: Int?
    var lockoutEnabled: Bool?
    var lockoutEndDateUtc: String?
    var passwordHash: String?
    var phoneNumber: String?
    var phoneNumberConfirmed: Bool?
    var username: String?
    var name: String?
    var surname: String?
    var marketing: Bool?
    var token: String?
    var securityStamp: String?
    var agencyId: Int?
    var posId: Int?
    var aGEN_ADR: String?
    var aGEN_NAZ: String?
    var aGEN_OIB: String?
    var facebookId: String?
    var papirTipSIF: Int?
    var aGEN_VLASTITA: Bool?
    var vLTA_OZN: String?
    var vLTA_SIF: Int?
    var acceptTerms: Bool?
    var tableName: String?
    
    required init(json: JSON) {
        googleId = json["Google_Id"].string
        email = json["Email"].string
        emailConfirmed = json["EmailConfirmed"].boolValue
        accessFailedCount = json["AccessFailedCount"].intValue
        id = json["Id"].intValue
        lockoutEnabled = json["LockoutEnabled"].boolValue
        lockoutEndDateUtc = json["LockoutEndDateUtc"].string
        passwordHash = json["PasswordHash"].string
        phoneNumber = json["PhoneNumber"].string
        phoneNumberConfirmed = json["PhoneNumberConfirmed"].boolValue
        username = json["Username"].string
        name = json["Name"].string
        surname = json["Surname"].string
        marketing = json["Marketing"].boolValue
        token = json["Token"].string
        securityStamp = json["SecurityStamp"].string
        agencyId = json["Agency_Id"].intValue
        posId = json["Pos_Id"].intValue
        aGEN_ADR = json["AGEN_ADR"].string
        aGEN_NAZ = json["AGEN_NAZ"].string
        aGEN_OIB = json["AGEN_OIB"].string
        facebookId = json["Facebook_Id"].string
        papirTipSIF = json["PAPIR_TIP_SIF"].intValue
        aGEN_VLASTITA = json["AGEN_VLASTITA"].boolValue
        vLTA_OZN = json["VLTA_OZN"].string
        vLTA_SIF = json["VLTA_SIF"].intValue
    }
    
    init(_acceptTerms: Bool, _email: String, _emailConfirmed: Bool, _accessFailedCount: Int, _id: Int, _lockoutEnabled: Bool, _passwordHash: String, _phoneNumber: String, _phoneNumberConfirmed: Bool, _username: String, _name: String, _surname: String, _marketing: Bool, _token: String, _agencyId: Int, _posId: Int) {
        
        self.acceptTerms = _acceptTerms
        self.email = _email
        self.emailConfirmed = _emailConfirmed
        self.accessFailedCount = _accessFailedCount
        self.id = _id
        self.lockoutEnabled = _lockoutEnabled
        self.passwordHash = _passwordHash
        self.phoneNumber = _phoneNumber
        self.phoneNumberConfirmed = _phoneNumberConfirmed
        self.username = _username
        self.name = _name
        self.surname = _surname
        self.marketing = _marketing
        self.token = _token
        self.agencyId = _agencyId
        self.posId = _posId
    }
    
    init(_agenAdr: String, _agenNaz: String, _agenOib: String, _tableName:String, _securityStamp: String, _lockoutEndDateUtc: String, _acceptTerms: Bool, _email: String, _emailConfirmed: Bool, _accessFailedCount: Int, _id: Int, _lockoutEnabled: Bool, _passwordHash: String, _phoneNumber: String, _phoneNumberConfirmed: Bool, _username: String, _name: String, _surname: String, _marketing: Bool, _token: String, _agencyId: Int, _posId: Int) {
        
        self.aGEN_ADR = _agenAdr
        self.aGEN_NAZ = _agenNaz
        self.aGEN_OIB = _agenOib
        self.tableName = _tableName
        self.lockoutEndDateUtc = _lockoutEndDateUtc
        self.securityStamp = _securityStamp
        self.acceptTerms = _acceptTerms
        self.email = _email
        self.emailConfirmed = _emailConfirmed
        self.accessFailedCount = _accessFailedCount
        self.id = _id
        self.lockoutEnabled = _lockoutEnabled
        self.passwordHash = _passwordHash
        self.phoneNumber = _phoneNumber
        self.phoneNumberConfirmed = _phoneNumberConfirmed
        self.username = _username
        self.name = _name
        self.surname = _surname
        self.marketing = _marketing
        self.token = _token
        self.agencyId = _agencyId
        self.posId = _posId
    }
    
    func profileToJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let marketing = marketing {
            json["Marketing"] = marketing
        }
        
        if let username = username {
            json["Username"] = username
        }
        
        if let phoneNumber = phoneNumber {
            json["PhoneNumber"] = phoneNumber
        }
        
        if let securityStamp = securityStamp {
            json["SecurityStamp"] = securityStamp
        }
        
        if let aGEN_ADR = aGEN_ADR {
            json["AGEN_ADR"] = aGEN_ADR
        }
        
        if let aGEN_NAZ = aGEN_NAZ {
            json["AGEN_NAZ"] = aGEN_NAZ
        }
        
        if let aGEN_OIB = aGEN_OIB {
            json["AGEN_OIB"] = aGEN_OIB
        }
        
        if let tableName = tableName {
            json["tableName"] = tableName
        }
        
        if let id = id {
            json["UserId"] = id
        }
        
        if let acceptTerms = acceptTerms {
            json["AcceptThermsOfUse"] = acceptTerms
        }
        
        if let email = email {
            json["Email"] = email
        }
        
        if let emailConfirmed = emailConfirmed {
            json["EmailConfirmed"] = emailConfirmed
        }
        
        if let accessFailedCount = accessFailedCount {
            json["AccessFailedCount"] = accessFailedCount
        }
        
        if let lockoutEndDate = lockoutEndDateUtc {
            json["LockoutEndDateUtc"] = lockoutEndDate
        }
        
        if let name = name {
            json["Name"] = name
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let surname = surname {
            json["Surname"] = surname
        }
        
        if let agency_Id = agencyId {
            json["Agency_Id"] = agency_Id
        }
        
        if let passwordHash = passwordHash {
            json["PasswordHash"] = passwordHash
        }
        
        if let phoneConf = phoneNumberConfirmed {
            json["PhoneNumberConfirmed"] = phoneConf
        }
        
        
        if let lockOutEnable = lockoutEnabled {
            json["LockoutEnabled"] = lockOutEnable
        }
        
        if let pos_Id = posId {
            json["Pos_Id"] = pos_Id
        }
        
        return json
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
 
        if let phone_num = phoneNumber {
            json["PhoneNumber"] = phone_num
        }
        
        if let email = email {
            json["Email"] = email
        }
        
        if let marketing = marketing {
            json["Marketing"] = marketing
        }
        
        if let username = username {
            json["Username"] = username
        }

        if let emailConf = emailConfirmed {
            json["EmailConfirmed"] = emailConf
        }
        
        if let id = id {
            json["Id"] = id
        }
        
        if let accesFaild = accessFailedCount {
            json["AccessFailedCount"] = accesFaild
        }

        if let name = name {
            json["Name"] = name
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let surname = surname {
            json["Surname"] = surname
        }

        if let agencyId = agencyId {
            json["Agency_Id"] = agencyId
        }

        if let passwordHash = passwordHash {
            json["PasswordHash"] = passwordHash
        }
        
        if let acceptTerms = acceptTerms {
            json["AcceptThermsOfUse"] = acceptTerms
        }
        
        if let phoneConf = phoneNumberConfirmed {
            json["PhoneNumberConfirmed"] = phoneConf
        }

        
        if let lockOutEnable = lockoutEnabled {
            json["LockoutEnabled"] = lockOutEnable
        }
        
        if let posId = posId {
            json["Pos_Id"] = posId
        }
        
        return json
    }
    
    override init() {
        super.init()
    }
}
