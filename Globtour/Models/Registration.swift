//
//  Registration.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/12/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Registration: NSObject, JSONDecodable {
    var id: Int?
    var email: String?
    var emailConfirmed: Bool?
    var accessFailedCount: Int?
    var lockoutEnabled: Bool?
    var passwordHash: String?
    var phoneNumber: String?
    var phoneNumberConfirmed: Bool?
    var username: String?
    var name: String?
    var surname: String?
    var marketing: Bool?
    var token: String?
    var securityStamp: String?
    var agency_Id: Int?
    var pos_Id: Int?
    var aGEN_ADR: String?
    var aGEN_NAZ: String?
    var aGEN_OIB: String?
    
    required init(json: JSON) {
        id = json["Id"].intValue
        email = json["Email"].string
        emailConfirmed = json["EmailConfirmed"].bool
        accessFailedCount = json["AccessFailedCount"].intValue
        lockoutEnabled = json["LockoutEnabled"].bool
        passwordHash = json["PasswordHash"].string
        phoneNumber = json["PhoneNumber"].string
        phoneNumberConfirmed = json["PhoneNumberConfirmed"].bool
        username = json["Username"].string
        name = json["Name"].string
        surname = json["Surname"].string
        marketing = json["Marketing"].bool
        token = json["Token"].string
        securityStamp = json["SecurityStamp"].string
        agency_Id = json["Agency_Id"].intValue
        pos_Id = json["Pos_Id"].intValue
        aGEN_ADR = json["AGEN_ADR"].string
        aGEN_NAZ = json["AGEN_NAZ"].string
        aGEN_OIB = json["AGEN_OIB"].string
    }
    
    override init() {
        super.init()
    }
}


