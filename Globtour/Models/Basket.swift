//
//  Basket.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/31/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Basket: NSObject, JSONDecodable {
    var myBasketId: Int?
    var racTmpCode: Int?
    
    required init(json: JSON) {
        myBasketId = json["my_basket_id"].intValue
        racTmpCode = json["RAC_TMP_CODE"].intValue
    }
    
    override init() {
        super.init()
    }
}
