//
//  PaymentCommitedInvoiceRequest.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/1/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation

class PaymentCommitedInvoiceRequest: NSObject {
    var approvalCode: String?
    var cardNum: String?
    var faktAdr: String?
    var faktCompany: String?
    var faktName: String?
    var faktOib: String?
    var faktR1: Bool?
    var posId: Int?
    var racTmpCode: Int?
    var softwareVersion: String?
    var token: String?
    var userId: Int?
    var isGroup: Bool?
    var myBasketId: Int?
    
    init(racTmpCode: Int, myBasketId: Int, userId: Int, posId: Int, softwareVersion: String, token: String, faktR1: Bool, isGroup: Bool, faktName: String, faktAdr: String, faktCompany: String, faktOib: String, approvalCode: String, cardNum: String) {
        self.racTmpCode = racTmpCode
        self.myBasketId = myBasketId
        self.userId = userId
        self.posId = posId
        self.softwareVersion = softwareVersion
        self.token = token
        self.faktR1 = faktR1
        self.isGroup = isGroup
        self.faktName = faktName
        self.faktAdr = faktAdr
        self.faktCompany = faktCompany
        self.faktOib = faktOib
        self.approvalCode = approvalCode
        self.cardNum = cardNum
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let approvalCode = approvalCode {
            json["APPROVAL_CODE"] = approvalCode
        }
        
        if let cardNum = cardNum {
            json["CARD_NUM"] = cardNum
        }
        
        if let faktAdr = faktAdr {
            json["FAKT_ADR"] = faktAdr
        }
        
        if let faktCompany = faktCompany {
            json["FAKT_COMPANY"] = faktCompany
        }
        
        if let faktName = faktName {
            json["FAKT_NAME"] = faktName
        }
        
        if let faktOib = faktOib {
            json["FAKT_OIB"] = faktOib
        }
        
        if let faktR1 = faktR1 {
            json["FAKT_R1"] = faktR1
        }
        
        if let posId = posId {
            json["Pos_ID"] = posId
        }
        
        if let racTmpCode = racTmpCode {
            json["RAC_TMP_CODE"] = racTmpCode
        }
        
        if let softwareVersion = softwareVersion {
            json["SoftwareVersion"] = softwareVersion
        }
        
        if let userId = userId {
            json["User_Id"] = userId
        }
        
        if let isGroup = isGroup {
            json["isGroup"] = isGroup
        }
        
        if let myBasketId = myBasketId {
            json["my_basket_id"] = myBasketId
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        return json
    }
}


