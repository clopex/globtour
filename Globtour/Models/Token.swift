//
//  Token.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/12/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Token: NSObject, NSCoding, JSONDecodable {
    
    private static var _sharedLogingSession: Token?
    static var shared: Token! {
        if _sharedLogingSession == nil {
            if let loginSessionRaw = UserDefaults.standard.data(forKey: "login_session") {
                if let loginSession = NSKeyedUnarchiver.unarchiveObject(with: loginSessionRaw) as? Token {
                    return loginSession
                }
                preconditionFailure("Invalid data for login_session userDefaults")
            }
        }
        return _sharedLogingSession
    }
    
    var id: Int!
    var username: String!
    var password: String!
    var token: String!
    var tokenPristup: String!
    var active: Bool!
    
    private override init() {
        token = nil
        id = nil
        username = nil
        password = nil
        tokenPristup = nil
        active = nil
    }
    
    required init(json: JSON) {
        id = json["Id"].intValue
        username = json["Username"].string
        password = json["Password"].string
        token = json["Token"].string
        tokenPristup = json["TokenPristup"].string
        active = json["Active"].bool
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "Id") as! Int
        username = aDecoder.decodeObject(forKey: "Username") as! String
        password = aDecoder.decodeObject(forKey: "Password") as! String
        token = aDecoder.decodeObject(forKey: "Token") as! String
        tokenPristup = aDecoder.decodeObject(forKey: "TokenPristup") as! String
        active = aDecoder.decodeObject(forKey: "Active") as! Bool
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "Id")
        aCoder.encode(username, forKey: "Username")
        aCoder.encode(password, forKey: "Password")
        aCoder.encode(token, forKey: "Token")
        aCoder.encode(tokenPristup, forKey: "TokenPristup")
        aCoder.encode(active, forKey: "Active")
    }
}

