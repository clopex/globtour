//
//  Station.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/8/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Station: NSObject, JSONDecodable {
    var id: Int?
    var name: String?
    var lang: String?
    var stationGpsX: Double?
    var stationGpsY: Double?
    
    required init(json: JSON) {
        id = json["Id"].intValue
        name = json["Name"].string
        lang = json["Lang"].string
        stationGpsX = json["STAN_ADR_GPSX"].doubleValue
        stationGpsY = json["STAN_ADR_GPSY"].doubleValue
    }
    
    override init() {
        super.init()
    }
}
