//
//  BaseDeparture.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/14/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class BaseDeparture: NSObject, JSONDecodable {
    var allDepartures: [Departure] = []
    var allReturns: [Departure] = []
    var index = 0
    
    required init(json: JSON) {
        
        for item in json.arrayValue {
            if index == 0 {
                for x in item.arrayValue {
                    let itemFromJson = Departure(json: x)
                    
                    allDepartures.append(itemFromJson)
                }
            }
            
            if index == 1 {
                for x in item.arrayValue {
                    let itemFromJson = Departure(json: x)
                    allReturns.append(itemFromJson)
                }
            }
            index = 1
        }
        
        //print(allReturns.count)
        //print(allDepartures.count)
        
//        for item in json.arrayValue {
////            let mainJson = Departure(json: item)
////            allReturns.append(mainJson)
//            for x in item.arrayValue {
//                let itemFromJson = Departure(json: x)
//                allDepartures.append(itemFromJson)
//            }
//        }
    }
    
    override init() {
        super.init()
    }
}

