//
//  Name.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/13/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Name: NSObject, JSONDecodable {
    var key: String?
    var value: String?
    
    required init(json: JSON) {
        key = json["Key"].string
        value = json["Value"].string
    }
    
    override init() {
        super.init()
    }
}
