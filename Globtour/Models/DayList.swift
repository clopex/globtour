//
//  DayList.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/31/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation

class DayList: NSObject {
    var date: Date!
    var isSelected: Bool!
    
    init(date: Date, isSelected: Bool) {
        self.date = date
        self.isSelected = isSelected
    }
}
