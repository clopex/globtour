//
//  RequestTripModel.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/12/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation

struct RequestModel: Encodable, Decodable {
    let _lang: String
    let _stationFrom: Int
    let _stationTo: Int
    let _dateTrip: String
    var _dateReturn: String
    let _isReturn: Bool
    let _isReturnDateKnown: Bool
    let _currency: Int
    let _token: String
    
    init(lang: String, stationFrom: Int, stationTo: Int, dateTrip: String, dateReturn: String, isReturn: Bool, isReturnDateKnown: Bool, currency: Int, token: String) {
        _lang = lang
        _stationFrom = stationFrom
        _stationTo = stationTo
        _dateTrip = dateTrip
        _dateReturn = dateReturn
        _isReturn = isReturn
        _isReturnDateKnown = isReturnDateKnown
        _currency = currency
        _token = token
    }
}
