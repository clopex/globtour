//
//  JSONEncodable.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/8/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation

protocol JSONEncodable {
    
    func toJsonString()
    
}
