//
//  WsPaySend.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/1/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class WsPaySend: NSObject, JSONDecodable {
    var cardVerificationData: String?
    var creditCardName: String?
    var creditCardNumber: String?
    var customerAddress: String?
    var customerCity: String?
    var customerCountry: String?
    var customerEmail: String?
    var customerFirstName: String?
    var customerLastName: String?
    var customerPhone: String?
    var customerZip: String?
    var dateTime: String?
    var expirationDate: String?
    var isTokenRequest: Bool?
    var paymenyPlan: String?
    var racTmpCode: Int?
    var returnErrorUrl: String?
    var returnUrl: String?
    var shopId: String?
    var shoppingCartId: String?
    var signature: String?
    var token: String?
    var totalAmount: String?
    var userId: Int?
    var myBasketId: Int?
    
    required init(json: JSON) {
        totalAmount = json["TotalAmount"].string
        myBasketId = json["my_basket_id"].intValue
        customerLastName = json["CustomerLastName"].string
        isTokenRequest = json["IsTokenRequest"].bool
        token = json["Token"].string
        userId = json["User_Id"].intValue
        customerCountry = json["CustomerCountry"].string
        customerPhone = json["CustomerPhone"].string
        returnErrorUrl = json["ReturnErrorURL"].string
        creditCardName = json["CreditCardName"].string
        racTmpCode = json["RAC_TMP_CODE"].intValue
        customerAddress = json["CustomerAddress"].string
        signature = json["Signature"].string
        paymenyPlan = json["PaymentPlan"].string
        customerEmail = json["CustomerEmail"].string
        expirationDate = json["ExpirationDate"].string
        customerCity = json["CustomerCity"].string
        creditCardNumber = json["CreditCardNumber"].string
        shoppingCartId = json["ShoppingCartID"].string
        customerZip = json["CustomerZIP"].string
        cardVerificationData = json["CardVerificationData"].string
        shopId = json["ShopID"].string
        returnUrl = json["ReturnURL"].string
        dateTime = json["DateTime"].string
        customerFirstName = json["CustomerFirstName"].string
    }
    
    init (paymenyPlan: String, totalAmount: String, dateTime: String, shopId: String, shoppingCartId: String, signature: String, customerAddress: String, customerCity: String, customerCountry: String, customerZip: String, customerEmail: String, customerPhone: String, token: String, userId: Int, racTmpCode: Int, myBasketId: Int, isTokenRequest: Bool, creditCardName: String, creditCardNumber: String, cardVerificationData: String, expirationDate: String, customerFirstName: String, customerLastName: String) {
        self.paymenyPlan = paymenyPlan
        self.totalAmount = totalAmount
        self.dateTime = dateTime
        self.shopId = shopId
        self.shoppingCartId = shoppingCartId
        self.signature = signature
        self.customerAddress = customerAddress
        self.customerCity = customerCity
        self.customerCountry = customerCountry
        self.customerZip = customerZip
        self.customerEmail = customerEmail
        self.customerPhone = customerPhone
        self.token = token
        self.userId = userId
        self.racTmpCode = racTmpCode
        self.myBasketId = myBasketId
        self.isTokenRequest = isTokenRequest
        self.creditCardName = creditCardName
        self.creditCardNumber = creditCardNumber
        self.cardVerificationData = cardVerificationData
        self.expirationDate = expirationDate
        self.customerFirstName = customerFirstName
        self.customerLastName = customerLastName
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let cardVerificationData = cardVerificationData {
            json["CardVerificationData"] = cardVerificationData
        }
        
        if let creditCardName = creditCardName {
            json["CreditCardName"] = creditCardName
        }
        
        if let creditCardNumber = creditCardNumber {
            json["CreditCardNumber"] = creditCardNumber
        }
        
        if let customerAddress = customerAddress {
            json["CustomerAddress"] = customerAddress
        }
        
        if let customerCity = customerCity {
            json["CustomerCity"] = customerCity
        }
        
        if let customerCountry = customerCountry {
            json["CustomerCountry"] = customerCountry
        }
        
        if let customerEmail = customerEmail {
            json["CustomerEmail"] = customerEmail
        }
        
        if let customerFirstName = customerFirstName {
            json["CustomerFirstName"] = customerFirstName
        }
        
        if let customerLastName = customerLastName {
            json["CustomerLastName"] = customerLastName
        }
        
        if let customerPhone = customerPhone {
            json["CustomerPhone"] = customerPhone
        }
        
        if let customerZip = customerZip {
            json["CustomerZIP"] = customerZip
        }
        
        if let dateTime = dateTime {
            json["DateTime"] = dateTime
        }
        
        if let expirationDate = expirationDate {
            json["ExpirationDate"] = expirationDate
        }
        
        if let paymenyPlan = paymenyPlan {
            json["PaymentPlan"] = paymenyPlan
        }
        
        
        if let isTokenRequest = isTokenRequest {
            json["IsTokenRequest"] = isTokenRequest
        }
        
        if let racTmpCode = racTmpCode {
            json["RAC_TMP_CODE"] = racTmpCode
        }
        
//        if let returnErrorUrl = returnErrorUrl {
//            json["ReturnErrorURL"] = returnErrorUrl
//        }
//
//        if let returnUrl = returnUrl {
//            json["ReturnURL"] = returnUrl
//        }
        
        if let shopId = shopId {
            json["ShopID"] = shopId
        }
        
        if let shoppingCartId = shoppingCartId {
            json["ShoppingCartID"] = shoppingCartId
        }
        
        if let signature = signature {
            json["Signature"] = signature
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let totalAmount = totalAmount {
            json["TotalAmount"] = totalAmount
        }
        
        if let userId = userId {
            json["User_Id"] = userId
        }
        
        if let myBasketId = myBasketId {
            json["my_basket_id"] = myBasketId
        }
        
        return json
    }
    
}










