//
//  PostPayment.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/1/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation

class PostPayment: NSObject {
    var email: String?
    var tokenNumber: String?
    var wsResponse: WsPayReceive?
    var invoiceRequest: PaymentCommitedInvoiceRequest?
    var wsPaySend: WsPaySend?
    
    init(email: String, tokenNumber: String, wsResponse: WsPayReceive, invoiceRequest: PaymentCommitedInvoiceRequest, wsPaySend: WsPaySend) {
        self.email = email
        self.tokenNumber = tokenNumber
        self.wsResponse = wsResponse
        self.invoiceRequest = invoiceRequest
        self.wsPaySend = wsPaySend
    }
}
