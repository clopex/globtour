//
//  PaymentCommitedInvoiceResponse.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/21/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class PaymentCommitedInvoiceResponse: NSObject, JSONDecodable {
    var posSif: Int?
    var faktBroj: Int?
    var year: Int?
    var pdf: [UInt8] = []
    var png: [UInt8] = []
    var invoicePdf: [UInt8] = []
    var invoicePng: [UInt8] = []
    var softwareVersion: String?
    var userId: Int?
    var token: String?
    var regeneratePdf: Bool?
    
    required init(json: JSON) {
        
        posSif = json["POS_SIF"].intValue
        faktBroj = json["FAKT_BRJ"].intValue
        year = json["Year"].intValue
        
        for item in json["PDF"].arrayValue {
            pdf.append(item.uInt8!)
        }
        
        for item in json["PNG"].arrayValue {
            png.append(item.uInt8!)
        }
        
        for item in json["InvoicePDF"].arrayValue {
            invoicePdf.append(item.uInt8!)
        }
        
        for item in json["InvoicePNG"].arrayValue {
            invoicePng.append(item.uInt8!)
        }
        
        softwareVersion = json["SoftwareVersion"].string
        userId = json["User_Id"].intValue
        token = json["Token"].string
        regeneratePdf = json["RegeneratePDF"].boolValue
    }
    
    init(posSif: Int, faktBroj: Int, year: Int, softwareVersion: String, userId: Int, token: String, regeneratePdf: Bool) {
        self.posSif = posSif
        self.faktBroj = faktBroj
        self.year = year
        self.softwareVersion = softwareVersion
        self.userId = userId
        self.token = token
        self.regeneratePdf = regeneratePdf
    }
    
    override init() {
        super.init()
    }
}
