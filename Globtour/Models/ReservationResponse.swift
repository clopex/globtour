//
//  ReservationResponse.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/9/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReservationResponse: NSObject, JSONDecodable {
    var error: String?
    var reserved: Bool?
    
    required init(json: JSON) {
        error = json["Error"].string
        reserved = json["Reserved"].boolValue
    }
    
    override init() {
        super.init()
    }
}
