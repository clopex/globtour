//
//  WsPayReceive.swift
//  Globtour
//
//  Created by Adis Mulabdic on 1/1/18.
//  Copyright © 2018 Adis Mulabdic. All rights reserved.
//

import Foundation

class WsPayReceive: NSObject {
    var approvalCode: String?
    var approved: String?
    var cavv: String?
    var creditCardName: String?
    var customerAddress: String?
    var customerCity: String?
    var customerCountry: String?
    var customerEmail: String?
    var customerFirstName: String?
    var customerLastName: String?
    var customerPhone: String?
    var customerZip: String?
    var eci: String?
    var errorMsg: String?
    var isTokenRequest: Bool?
    var maskedPlan: String?
    var paymentPlan: String?
    var shoppingCardId: String?
    var signature: String?
    var token: String?
    var totalAmount: String?
    var wsToken: String?
    var wsTokenNumber: String?
    var wsPayOrderId: String?
    var xid: String?
    
    init(approvalCode: String, approved: String, cavv: String, creditCardName: String, customerAddress: String, customerCity: String, customerCountry: String, customerEmail: String, customerFirstName: String, customerLastName: String, customerPhone: String, customerZip: String, eci: String, errorMsg: String, isTokenRequest: Bool, maskedPlan: String, paymentPlan: String, shoppingCardId: String, signature: String, token: String, totalAmount: String, wsToken: String, wsTokenNumber: String, wsPayOrderId: String, xid: String) {
        self.approvalCode = approvalCode
        self.approved = approved
        self.cavv = cavv
        self.creditCardName = creditCardName
        self.customerZip = customerZip
        self.customerAddress = customerAddress
        self.customerCity = customerCity
        self.customerCountry = customerCountry
        self.customerEmail = customerEmail
        self.customerFirstName = customerFirstName
        self.customerLastName = customerLastName
        self.customerPhone = customerPhone
        self.eci = eci
        self.errorMsg = errorMsg
        self.isTokenRequest = isTokenRequest
        self.maskedPlan = maskedPlan
        self.paymentPlan = paymentPlan
        self.shoppingCardId = shoppingCardId
        self.signature = signature
        self.token = token
        self.totalAmount = totalAmount
        self.wsToken = wsToken
        self.wsTokenNumber = wsTokenNumber
        self.wsPayOrderId = wsPayOrderId
        self.xid = xid
        
    }
    
    func toJSON() -> Dictionary<String, Any> {
        var json = Dictionary<String, Any>()
        
        if let approvalCode = approvalCode {
            json["ApprovalCode"] = approvalCode
        }
        
        if let cavv = cavv {
            json["CAVV"] = cavv
        }
        
        if let creditCardName = creditCardName {
            json["CreditCardName"] = creditCardName
        }
        
        if let approved = approved {
            json["Approved"] = approved
        }
        
        if let customerAddress = customerAddress {
            json["CustomerAddress"] = customerAddress
        }
        
        if let customerCity = customerCity {
            json["CustomerCity"] = customerCity
        }
        
        if let customerCountry = customerCountry {
            json["CustomerCountry"] = customerCountry
        }
        
        if let customerEmail = customerEmail {
            json["CustomerEmail"] = customerEmail
        }
        
        if let customerFirstName = customerFirstName {
            json["CustomerFirstName"] = customerFirstName
        }
        
        if let customerLastName = customerLastName {
            json["CustomerLastName"] = customerLastName
        }
        
        if let customerPhone = customerPhone {
            json["CustomerPhone"] = customerPhone
        }
        
        if let customerZip = customerZip {
            json["CustomerZIP"] = customerZip
        }
        
        if let eci = eci {
            json["ECI"] = eci
        }
        
        if let errorMsg = errorMsg {
            json["ErrorMessage"] = errorMsg
        }
        
        if let maskedPlan = maskedPlan {
            json["MaskedPan"] = maskedPlan
        }
        
        if let paymentPlan = paymentPlan {
            json["PaymentPlan"] = paymentPlan
        }
        
        if let isTokenRequest = isTokenRequest {
            json["IsTokenRequest"] = isTokenRequest
        }
        
        if let wsTokenNumber = wsTokenNumber {
            json["WSTokenNumber"] = wsTokenNumber
        }
        
        if let wsToken = wsToken {
            json["WSToken"] = wsToken
        }
        
        if let shoppingCardId = shoppingCardId {
            json["ShoppingCartID"] = shoppingCardId
        }
        
        if let signature = signature {
            json["Signature"] = signature
        }
        
        if let token = token {
            json["Token"] = token
        }
        
        if let totalAmount = totalAmount {
            json["TotalAmount"] = totalAmount
        }
        
        if let wsPayOrderId = wsPayOrderId {
            json["WsPayOrderId"] = wsPayOrderId
        }
        
        if let xid = xid {
            json["XID"] = xid
        }
        
        return json
    }
}


















