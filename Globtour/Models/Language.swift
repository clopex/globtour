//
//  Language.swift
//  Globtour
//
//  Created by Adis Mulabdic on 12/17/17.
//  Copyright © 2017 Adis Mulabdic. All rights reserved.
//

import Foundation
import SwiftyJSON

class Language: NSObject, JSONDecodable {
    var languageName: String?
    var languageSif: Int?
    var langaugeKey: String?
    var translation: String?
    
    required init(json: JSON) {
        languageName = json["JEZIK_NAZ"].string
        languageSif = json["JEZIK_SIF"].intValue
        langaugeKey = json["kljuc"].string
        translation = json["prijevod"].string
    }
    
    override init() {
        super.init()
    }
}
